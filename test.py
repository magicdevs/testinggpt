# -*- coding: utf-8 -*-
import multiprocessing
import asyncio
import asyncio_mongo
import asyncio_mongo.filter
import time
from asyncio_mongo._bson.objectid import ObjectId
from bson import ObjectId as PyObjectId
import aiohttp
import json
from collections import OrderedDict
import urllib.request as urlrequest
from urllib.parse import urlencode, urlparse, unquote, quote
import sys
import os
import base64
import traceback
import hashlib
import lxml.html as html
from lxml import etree
import html as html_html
import types
import pickle
from tldextract import extract as get_domain
from datetime import datetime, timedelta
import socket
from pymongo import MongoClient
import requests
from http.cookies import Morsel, SimpleCookie, BaseCookie
from pprint import pprint
import idna
from bs4 import BeautifulSoup
import re
import random
import pydub
from speech_recognition import Recognizer, AudioFile
###webdriver
from selenium import webdriver
# from seleniumwire import webdriver

import time
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

''' FIREFOX '''
from selenium.webdriver.firefox.options import Options as FirefoxOptions
''' FIREFOX '''

from python3_anticaptcha import NoCaptchaTaskProxyless, NoCaptchaTask, ImageToTextTask
from anticaptchaofficial.recaptchav2proxyless import *
from anticaptchaofficial.recaptchav2proxyon import *
from anticaptchaofficial.recaptchav2enterpriseproxyon import *

from random import uniform, randint, choice
import shutil
import signal

import zipfile

import urllib.request
from pathlib import Path

from fake_useragent import UserAgent
from twocaptcha import TwoCaptcha

from PIL import Image
from io import BytesIO

os.environ['GH_TOKEN'] = "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"

# with open('captcha_log.txt', 'w') as f:pass

class asyncioReports():
    def __init__(self):
        self.lock = asyncio.Lock()
        self.pool = []
        self.processes = []
        self.reports_in_work = {}
        self.check_completed_tasks = {}
        self.projects_data = {}
        self.max_processes = multiprocessing.cpu_count()
        self.max_processes = 2
        # self.max_processes = 15
        self.max_tasks_per_process = 100
        self.max_task_per_worker = 100
        # self.max_tasks_per_process = 40
        # self.max_task_per_worker = 40
#       self.max_tasks_per_process = 10
#       self.max_task_per_worker = 10
        self.sleep_after_loop = 1
        # self.sleep_after_query = 0.5
        self.sleep_after_query = 0.5
        self.sleep_after_continue = 10
        # self.sleep_after_query = 3

        self.proxy_cookies_dir = './proxy_cookies'
        if not os.path.isdir(self.proxy_cookies_dir):
            os.makedirs(self.proxy_cookies_dir)
        self.ANTICAPTCHA_KEY = 'e5ebc2bb0f61654782ca37d29c4c720b'

        self.PROXIES_LIST = [
            'squser:cW3jkyZScJs@46.21.146.229:4767',
            'squser:cW3jkyZScJs@46.21.146.115:4767',
            'squser:cW3jkyZScJs@46.21.146.149:4767',
            'squser:cW3jkyZScJs@46.21.146.252:4767',
            'squser:cW3jkyZScJs@94.100.22.39:4767',
            'squser:cW3jkyZScJs@94.100.22.61:4767',
            'squser:cW3jkyZScJs@94.100.24.165:4767',
            'squser:cW3jkyZScJs@94.100.24.190:4767',
            'squser:cW3jkyZScJs@94.100.29.163:4767',
            'squser:cW3jkyZScJs@94.100.29.189:4767',
            'squser:cW3jkyZScJs@31.3.241.195:7734',
            'squser:cW3jkyZScJs@31.3.241.216:7734',
            'squser:cW3jkyZScJs@37.220.1.118:7734',
            'squser:cW3jkyZScJs@31.3.253.117:7734',
            'squser:cW3jkyZScJs@37.220.3.131:7734',
            'squser:cW3jkyZScJs@5.152.200.102:7734',
            'squser:cW3jkyZScJs@5.152.199.230:7734',
            'squser:cW3jkyZScJs@37.157.245.229:7734',
            'squser:cW3jkyZScJs@37.220.11.98:7734',
            'squser:cW3jkyZScJs@5.152.221.194:7734',
            'squser:cW3jkyZScJs@78.46.127.229:6642',
            'squser:cW3jkyZScJs@78.46.127.242:6642',
            'squser:cW3jkyZScJs@136.243.120.35:6642',
            'squser:cW3jkyZScJs@136.243.120.57:6642',
            'squser:cW3jkyZScJs@88.198.220.98:6642',
            'squser:cW3jkyZScJs@88.198.220.106:6642',
            'squser:cW3jkyZScJs@78.46.232.218:6642',
            'squser:cW3jkyZScJs@78.47.1.220:6642',
            'squser:cW3jkyZScJs@88.198.94.53:6642',
            'squser:cW3jkyZScJs@78.47.1.196:6642',
        ]

        self.captcha_services = [
            {
                'service' : 'antigate',
                'url' : 'http://anti-captcha.com',
                'key' : 'e5ebc2bb0f61654782ca37d29c4c720b'
            },
            # {
            #   'service' : 'rucaptcha',
            #   'url' : 'http://rucaptcha.com',
            #   'key' : 'ad0d1f129d603b03114fe27b31e714b0'
            # }
        ]

        self.headers = {
            # 'Pragma': 'no-cache',
            'Accept-Encoding': 'gzip, deflate',

            # chrome
            # 'Accept-Language': 'ru,en;q=0.8,uk;q=0.6',
            # 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
            # 'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            
            # mozilla
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:55.0) Gecko/20100101 Firefox/55.0',
            'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',

            'Upgrade-Insecure-Requests': '1',
            'Cache-Control' : 'max-age=0',
            'Connection' : 'keep-alive',
        }

        self.manifest_json = """
        {
            "version": "1.0.0",
            "manifest_version": 2,
            "name": "Chrome Proxy",
            "permissions": [
                "proxy",
                "tabs",
                "unlimitedStorage",
                "storage",
                "<all_urls>",
                "webRequest",
                "webRequestBlocking"
            ],
            "background": {
                "scripts": ["background.js"]
            },
            "minimum_chrome_version":"22.0.0"
        }
        """

        self.background_js = """
        var config = {
                mode: "fixed_servers",
                rules: {
                singleProxy: {
                    scheme: "http",
                    host: "%s",
                    port: parseInt(%s)
                },
                bypassList: ["localhost"]
                }
            };

        chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

        function callbackFn(details) {
            return {
                authCredentials: {
                    username: "%s",
                    password: "%s"
                }
            };
        }

        chrome.webRequest.onAuthRequired.addListener(
                    callbackFn,
                    {urls: ["<all_urls>"]},
                    ['blocking']
        );
        """

        self.main_loop = asyncio.get_event_loop()
        # self.main_loop.set_debug(True)

        self.dbs = {}
        self.events = {}
        self.proxy_ip = 'localhost:2226'
        # self.pager_ip = '91.210.250.37'
        self.pager_ip = '78.46.232.201'
        # self.pager_id = 'localhost'
        self.positions_ip = '78.46.232.201'
        # self.positions_ip = 'localhost'

        # pager_olient = self.main_loop.run_until_complete(asyncio_mongo.Connection.create('91.210.250.37', 27017, loop = self.main_loop))
        self.dbs['pager'] = self.main_loop.run_until_complete(asyncio_mongo.Connection.create(self.pager_ip, 27017, loop = self.main_loop)).newshield
        # positions_client = self.main_loop.run_until_complete(asyncio_mongo.Connection.create('91.210.250.38', 27017, loop = self.main_loop))
        self.dbs['positions'] = self.main_loop.run_until_complete(asyncio_mongo.Connection.create(self.positions_ip, 27017, loop = self.main_loop)).newshield_positions

        asyncio.async(self.get_reports())
        asyncio.async(self.process_loop())

        self.main_loop.run_forever()
        self.main_loop.close()

    def acp_api_send_request(self, driver, message_type, data={}):
            message = {
                # всегда указывается именно этот получатель API сообщения
                'receiver': 'antiCaptchaPlugin',
                # тип запроса, например setOptions
                'type': message_type,
                # мерджим с дополнительными данными
                **data
            }
            # выполняем JS код на странице
            # а именно отправляем сообщение стандартным методом window.postMessage
            return driver.execute_script("""
            return window.postMessage({});
            """.format(json.dumps(message)))


    async def scroll_some(self, loop):
        s_keys = [Keys.ARROW_DOWN, Keys.PAGE_DOWN]
        body = self.driver.find_element(By.XPATH, '//body')
        for x in range(1,randint(3, 8)):
            key = choice(s_keys)
            body.send_keys(key)
            await self.wait_between(0.1, 0.5, loop)


    async def wait_between(self, a, b, loop):
        rand=uniform(a, b)
        await asyncio.sleep(rand, loop = loop)

    def hasmethod(self, obj, name):
        return hasattr(obj, name) and type(getattr(obj, name)) == types.MethodType

    # возвращает след. доступный прокси
    def get_available_proxy(self, group = 'all', name = 'positionsReports', returnHostPort = False):
        # connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # host = self.proxy_ip
        # host_parts = host.split(':')

        # params = host_parts[0], int(host_parts[1])
        # connection.connect(params)
        # connection.sendall('{0}:{1}'.format(group, name).encode('utf-8'))
        # data = connection.recv(1024)
        # connection.close()
        # if returnHostPort is True:
        #     data = data.decode('utf-8')
        # else:
        #     data = 'http://'+data.decode('utf-8')

        data = random.choice(self.PROXIES_LIST)
        self.PROXIES_LIST.remove(data)
        if len(self.PROXIES_LIST) == 0:
            self.PROXIES_LIST = [
                'squser:cW3jkyZScJs@46.21.146.229:4767',
                'squser:cW3jkyZScJs@46.21.146.115:4767',
                'squser:cW3jkyZScJs@46.21.146.149:4767',
                'squser:cW3jkyZScJs@46.21.146.252:4767',
                'squser:cW3jkyZScJs@94.100.22.39:4767',
                'squser:cW3jkyZScJs@94.100.22.61:4767',
                'squser:cW3jkyZScJs@94.100.24.165:4767',
                'squser:cW3jkyZScJs@94.100.24.190:4767',
                'squser:cW3jkyZScJs@94.100.29.163:4767',
                'squser:cW3jkyZScJs@94.100.29.189:4767',
                'squser:cW3jkyZScJs@31.3.241.195:7734',
                'squser:cW3jkyZScJs@31.3.241.216:7734',
                'squser:cW3jkyZScJs@37.220.1.118:7734',
                'squser:cW3jkyZScJs@31.3.253.117:7734',
                'squser:cW3jkyZScJs@37.220.3.131:7734',
                'squser:cW3jkyZScJs@5.152.200.102:7734',
                'squser:cW3jkyZScJs@5.152.199.230:7734',
                'squser:cW3jkyZScJs@37.157.245.229:7734',
                'squser:cW3jkyZScJs@37.220.11.98:7734',
                'squser:cW3jkyZScJs@5.152.221.194:7734',
                'squser:cW3jkyZScJs@78.46.127.229:6642',
                'squser:cW3jkyZScJs@78.46.127.242:6642',
                'squser:cW3jkyZScJs@136.243.120.35:6642',
                'squser:cW3jkyZScJs@136.243.120.57:6642',
                'squser:cW3jkyZScJs@88.198.220.98:6642',
                'squser:cW3jkyZScJs@88.198.220.106:6642',
                'squser:cW3jkyZScJs@78.46.232.218:6642',
                'squser:cW3jkyZScJs@78.47.1.220:6642',
                'squser:cW3jkyZScJs@88.198.94.53:6642',
                'squser:cW3jkyZScJs@78.47.1.196:6642',
            ]

        print('\t\t\t\t\t\t\t\t\t\t\t GET PROXY - {0}'.format(data))
        return data

    def chunks(self, items, chunk_size):
        for i in range(0, len(items), chunk_size):
            yield items[i:i + chunk_size]

    def prepare_url(self, se_name, se_data, region, keyword):
        scheme = 'https'
        query = ''
        params = OrderedDict()
        if se_name == 'google':
            url = 'www.google.'+se_data['se_domain']+'/search?'
            params['q'] = keyword
            params['num'] = 100

        elif se_name == 'yandex':
            url = 'yandex.'+se_data['se_domain']+'/search/?'
            params['text'] = keyword
            try:
                params['lr'] = se_data['se_country']
            except:
                params['lr'] = '143'

        query = '{scheme}://{url}{get_parameters}'.format(scheme=scheme, url=url, get_parameters=urlencode(params))

        return query

    async def parse_main(self, se_name, content):
        add_params = {}
        try:
            try:
                tree = html.fromstring(content)
            except:
                try:
                    tree = html.fromstring(BeautifulSoup(content, 'html.parser').prettify())
                except:
                    pass

            tmp_inputs = tree.xpath('//input[@type="hidden"]')
            if len(tmp_inputs) > 0:
                for tmp_i in tmp_inputs:
                    tmp_n = tmp_i.xpath('@name')
                    tmp_v = tmp_i.xpath('@value')
                    if len(tmp_v) > 0 and len(tmp_n) > 0:
                        tmp_n = tmp_n[0]
                        tmp_v = tmp_v[0]
                        if len(tmp_v) > 0 and len(tmp_n) > 0:
                            if tmp_n == 'lr':continue
                            add_params[tmp_n] = tmp_v
        except:
            pass

        if len(add_params) > 0:
            add_params = '&' + urlencode(add_params)
        else:
            add_params = ''

        return add_params

    def check_empty_response(self, se_name, content, loop):
        try:
            content = content.encode('utf-8')
            try:
                tree = html.fromstring(content)
            except:
                try:
                    tree = html.fromstring(BeautifulSoup(content, 'html.parser').prettify())
                except:
                    pass

            if se_name == 'google':
                empty_block = tree.xpath('//div[@id="topstuff"]')
                for eb in empty_block:
                    if len(eb.text_content()) != 0:
                        return True
#               print(etree.tostring(tree.xpath('//html')[0]).decode('utf-8'))
#               rint(tree.xpath('//body')[0].text_content())
            elif se_name == 'yandex':
                pass

            return False
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            tb = traceback.format_exc()
            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~BREAD~~~~~~~~~~~~~")
            print(exc_type, fname, exc_tb.tb_lineno, tb)
            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~BREAD~~~~~~~~~~~~~")
            return False

    def parse_response(self, se_name, loop, task_data):
        WebDriverWait(self.driver, 30).until(lambda driver: self.driver.execute_script('return document.readyState') == 'complete')
        content = self.driver.page_source
        results = []
        #content = content.encode('utf-8')
        try:
            tree = html.fromstring(content)
        except:
            print("~~~LXM FUCKED UP~~~~")
            try:
                tree = html.fromstring(BeautifulSoup(content, 'html.parser').prettify())
            except:
                pass
        if se_name == 'google':
            if len(tree.xpath('//*[@id="rso"]/div')) > 0:
                for (position, block) in enumerate(tree.xpath('//*[@id="rso"]/div')):
                    if block.xpath('contains(@class, "g")') is not True:
                        if 'WEB_ANSWERS' in html.tostring(block).decode('utf-8'):
                            continue
                        block = block.xpath('.//div[contains(@class, "g")]')
                        if len(block) > 0:
                            block = block[0]
                        else:
                            continue
                    if len(block.xpath('.//div/div/a')) > 0:
                            a = block.xpath('.//div/div/a')[0]
                            if len(a.xpath('h3')) == 0:
                                continue
                            href = a.xpath('@href')[0]
                            if len(a.xpath('.//h3/span')) == 0:
                                anchor = a.xpath('.//h3')[0].text_content()
                            else:
                                anchor = a.xpath('.//h3/span')[0].text_content()
                            em_blocks = block.xpath('.//em')
                            
                            ems = []
                            for em in em_blocks:
                                em_text = em.text_content()
                                em_text = em_text.lower().strip()
                                if em_text == '...': continue
                                ems.append(em_text)
                            ems = list(set(ems))
                            results.append({
                                'href' : href,
                                'anchor' : anchor,
                                'em' : ems,
                            })
            if len(tree.xpath('//div[@id="search"]//div[@class="g"]')) > 0 and len(results) == 0:
                for (position, block) in enumerate(tree.xpath('//div[@id="search"]//div[@class="g"]')):
                    if len(block.xpath('.//div/div/a')) > 0:
                        a = block.xpath('.//div/div/a')[0]
                        if len(a.xpath('h3')) == 0:
                            continue
                        href = a.xpath('@href')[0]
                        if len(a.xpath('.//h3/span')) == 0:
                            anchor = a.xpath('.//h3')[0].text_content()
                        else:
                            anchor = a.xpath('.//h3/span')[0].text_content()
                        em_blocks = block.xpath('.//div/div/div/span/span/em')
                        
                        ems = []
                        for em in em_blocks:
                            em_text = em.text_content()
                            em_text = em_text.lower().strip()
                            if em_text == '...': continue
                            ems.append(em_text)
                        ems = list(set(ems))
                        results.append({
                            'href' : href,
                            'anchor' : anchor,
                            'em' : ems,
                        })
            if len(tree.xpath("//div[@class='rc']")) > 0  and len(results) == 0:
                for (position, block) in enumerate(tree.xpath("//div[@class='rc']")):
                    if len(block.xpath('h3/a')) > 0:
                        a = block.xpath('h3/a')[0]
                        href = a.xpath('@href')[0]
                        anchor = a.text_content()

                        em_blocks = block.xpath('div/div/span/em')
                        
                        ems = []
                        for em in em_blocks:
                            em_text = em.text_content()
                            em_text = em_text.lower().strip()
                            if em_text == '...': continue
                            ems.append(em_text)
                        ems = list(set(ems))
                        results.append({
                            'href' : href,
                            'anchor' : anchor,
                            'em' : ems,
                        })
                    elif len(block.xpath('div/a')) > 0:
                        a = block.xpath('div/a')[0]
                        href = a.xpath('@href')[0]
                        anchor = a.xpath('h3')[0].text_content()
                        em_blocks = block.xpath('div/div/span/em')
                        ems = []
                        for em in em_blocks:
                            em_text = em.text_content()
                            em_text = em_text.lower().strip()
                            if em_text == '...': continue
                            ems.append(em_text)
                        ems = list(set(ems))
                        results.append({
                            'href' : href,
                            'anchor' : anchor,
                            'em' : ems,
                        })
            if len(tree.xpath('//div[@id="search"]/div/div/div[@class="g"]')) > 0 and len(results) == 0:
                for (position, block) in enumerate(tree.xpath('//div[@id="search"]/div/div/div[@class="g"]')):
                    if len(block.xpath('h3/a')) > 0:
                        a = block.xpath('h3/a')[0]
                        href = a.xpath('@href')[0]
                        anchor = a.text_content()

                        em_blocks = block.xpath('div/div/span/em')
                        
                        ems = []
                        for em in em_blocks:
                            em_text = em.text_content()
                            em_text = em_text.lower().strip()
                            if em_text == '...': continue
                            ems.append(em_text)
                        ems = list(set(ems))
                        results.append({
                            'href' : href,
                            'anchor' : anchor,
                            'em' : ems,
                        })
                    elif len(block.xpath('div/a')) > 0:
                        a = block.xpath('div/a')[0]
                        href = a.xpath('@href')[0]
                        anchor = a.xpath('h3')[0].text_content()
                        em_blocks = block.xpath('div/div/span/em')
                        ems = []
                        for em in em_blocks:
                            em_text = em.text_content()
                            em_text = em_text.lower().strip()
                            if em_text == '...': continue
                            ems.append(em_text)
                        ems = list(set(ems))
                        results.append({
                            'href' : href,
                            'anchor' : anchor,
                            'em' : ems,
                        })

            if len(tree.xpath('//div[@id="search"]/div/div/div/div[@class="g"]')) > 0  and len(results) == 0:
                for (position, block) in enumerate(tree.xpath('//div[@id="search"]/div/div/div/div[@class="g"]')):
                    if len(block.xpath('div/div/a')) > 0:
                        a = block.xpath('div/div/a')[0]
                        href = a.xpath('@href')[0]
                        anchor = a.xpath('h3/span')[0].text_content()
                        em_blocks = block.xpath('div/div/div/span/span/em')
                        
                        ems = []
                        for em in em_blocks:
                            em_text = em.text_content()
                            em_text = em_text.lower().strip()
                            if em_text == '...': continue
                            ems.append(em_text)
                        ems = list(set(ems))
                        results.append({
                            'href' : href,
                            'anchor' : anchor,
                            'em' : ems,
                        })
        elif se_name == 'yandex':
            for block in tree.xpath("//*[contains(@class, 'serp-item') and not(contains(@class, 'serp-adv-item') or contains(@class, 'z-images') or contains(@class, 'z-video') or contains(@class, 'z-companies') or contains(@class, 'template-fact'))][@data-cid][not(descendant::div[contains(@class, 'label')])][descendant::div/h2/a]"):
                try:
                    a = block.xpath('div/h2/a')[0]
                except IndexError:
                    continue

                em_blocks = block.xpath('div/div/div/b')
                href = a.xpath('@href')[0]
                anchor = a.text_content()
                ems = []
                for em in em_blocks:
                    em_text = em.text_content()
                    em_text = em_text.lower().strip()
                    if em_text == '...': continue
                    ems.append(em_text)
                ems = list(set(ems))
                results.append({
                    'href' : href,
                    'anchor' : anchor,
                    'em' : ems
                })

        return results

    def prepare_results(self, results, project):
        find_with_domain = False
        project_domains = project['domains']
        for (i, data) in enumerate(results):
            new_data = data
            position = i + 1
            new_data['position'] = position
            href_url = new_data['href']
            parsed_href = urlparse(href_url)
            href_domain = parsed_href.netloc
            try:
                href_domain = idna.decode(href_domain)
            except:
                pass
            uri = str(parsed_href.path)
            if parsed_href.query != '':
                uri += '?' + str(parsed_href.query)
            url = unquote(uri)
            del new_data['href']
            new_data['url'] = url
            new_data['toplevel_domain'] = href_domain
            new_data['domain'] = get_domain(href_domain).domain + '.' + get_domain(href_domain).suffix
            new_data['scheme'] = 'http'
            try:
                new_data['scheme'] = parsed_href.scheme
            except:
                pass
            for project_domain in project_domains:
                project_domain = project_domain.replace('http://', '')
                project_domain = project_domain.replace('https://', '')
                project_domain = re.sub('\/+$', '', project_domain)
                if project_domain.lower() == new_data['toplevel_domain'].lower():
                    new_data['find_with_domain'] = True
                    new_data['find_without_domains'] = True
                    break
                elif '.' + project_domain.lower() in new_data['toplevel_domain'].lower() and find_with_domain is False:
                    new_data['find_with_domain'] = True
                    find_with_domain = True

            if new_data['domain'].lower() == new_data['toplevel_domain'].lower():
                del new_data['domain']
            results[i] = new_data
        return results

    async def get_reports(self):
        l_name = 'get_reports'

        with await self.lock:
            reports_in_work_ids = list(self.reports_in_work.keys())
            for (k,v) in enumerate(reports_in_work_ids):
                reports_in_work_ids[k] = ObjectId(v)

        # #TMPTMP
        project_ids = [
                #ObjectId("60d07c81c708cc47df7b9af7"),
        ]

        # reports = await self.dbs['positions'].reports.find({'_id' : {'$in' : [ObjectId("604f0e1a480c0674e74d67e8")]}, 'status' : 'new'}, filter = asyncio_mongo.filter.sort(asyncio_mongo.filter.ASCENDING("date_add")))
        if len(project_ids) > 0:
            reports = await self.dbs['positions'].reports.find({'_id' : {'$nin' : reports_in_work_ids}, 'status' : 'new', 'project_id' : {'$in' : project_ids}}, filter = asyncio_mongo.filter.sort(asyncio_mongo.filter.ASCENDING("date_add")))
            for report in reports:
                project = await self.dbs['positions'].trees.find_one({'_id' : report['project_id']})
                if 'keywords' not in project:
                    report['keys_count'] = 0
                else:
                    report['keys_count'] = len(project['keywords'])
            reports = sorted(reports, key = lambda k : k['keys_count'])
            reports = reports[0:2]
        elif len(reports_in_work_ids) < 5:
            reports = await self.dbs['positions'].reports.find({'_id' : {'$nin' : reports_in_work_ids}, 'status' : 'new'}, filter = asyncio_mongo.filter.sort(asyncio_mongo.filter.ASCENDING("date_add")))
            for report in reports:
               project = await self.dbs['positions'].trees.find_one({'_id' : report['project_id']})
               if 'keywords' not in project:
                   report['keys_count'] = 0
               else:
                   report['keys_count'] = len(project['keywords'])
            reports = sorted(reports, key = lambda k : k['keys_count'])
            reports = reports[0:2 - len(reports_in_work_ids)]
        else:
            reports = []
        # if len(reports_in_work_ids) > 0:
        #     reports = []
        for report in reports:
            await self.dbs['positions'].reports.update({'_id' : report['_id']}, {'$set' : {'date_start' : datetime.utcnow()}}, safe = True)
            project = await self.dbs['positions'].trees.find_one({'_id' : report['project_id']})
            if project == {}:
                project = await self.dbs['pager'].trees.find_one({'_id' : report['project_id']})
                if 'connected_servicess' not in project: continue
                project['domains'] = project['connected_servicess']['positions']['aliases']
                project['search_systems'] = project['connected_servicess']['positions']['search_systems']
                if project['type'] == 'subproject':
                    project['keywords'] = []
                    subproject_keywords = await self.dbs['pager'].subproject_keywords.find({'project_id' : report['project_id']})
                    if subproject_keywords is None:
                        print('!!!!!!!')
                        print(report)
                        print('!!!!!!')
                    for keyword in subproject_keywords:
                        project['keywords'].append(keyword['keyword_id'])
                else:
                    project['keywords'] = []
                    subproject_keywords = await self.dbs['pager'].trees.aggregate([
                        {
                            '$match' : {
                                '_id' : project['_id'],
                            }
                        },
                        {
                            '$project' : {
                                '_id' : 1
                            }
                        },
                        {
                            '$lookup' : {
                                'from' : 'landing_pages',
                                'as' : 'lp',
                                'localField' : '_id',
                                'foreignField' : 'project_id',
                            }
                        },
                        {
                            '$unwind' : '$lp',
                        },
                        {
                            '$unwind' : '$lp.keywords',
                        },
                        {
                            '$project' : {
                                '_id' : 1,
                                'keywords' : '$lp.keywords'
                            }
                        },
                        {
                            '$group' : {
                                '_id' : '$_id',
                                'keywords' : {'$addToSet' : '$keywords'}
                            }
                        },
                        {
                            '$unwind' : '$keywords',
                        },
                        ])
                    if subproject_keywords is None:
                        print('!!!!!')
                        print(project)
                        print('!!!!!')
                    #print(subproject_keywords)
                    for keyword in subproject_keywords:
                        project['keywords'].append(keyword['keywords'])
            if 'keywords' not in project or len(project['keywords']) == 0 or 'domains' not in project:
                print('No keywords in project {}'.format(','.join(project['domains'])))
                res = await self.dbs['positions'].reports.update({'_id' : ObjectId(str(report['_id']))}, {'$set' : {'status' : 'error'}})
                continue
            
            print(project['domains'])
            print("\tPREPARING PROJECT {0} - {1} keywords".format(','.join(project['domains']), len(project['keywords'])))
            # get keywords by limit
            i = 0
            keywords = []
            _limit = 10000
            while True:
                _start = i * _limit
                _end = _start + _limit
                if _start >= len(project['keywords']) : break
                i += 1

                print('\t\tget_keywords {}:{}'.format(_start, _end))
                keywords += list(await self.dbs['pager'].keywords.find({'_id' : {'$in' : project['keywords'][_start:_end]}}, fields = {'keyword' : 1}))

            print("\tGET completed_keywords_docs")
            completed_keywords_docs = await self.dbs['positions'].reports_data.find({'report_id' : report['_id']}, fields = {'keyword_id' : 1})
            completed_keywords = {}
            for ck in completed_keywords_docs:
                completed_keywords[str(ck['keyword_id'])] = 1

            kw_tasks = 0
            if len(keywords) != len(completed_keywords):
                for keyword in keywords:
                    if str(keyword['_id']) in completed_keywords:
                        continue
                    keyword = dict(keyword, **{'report_id' : str(report['_id'])})
                    if 'search_systems' not in project or project['search_systems'] is None:
                        res = await self.dbs['positions'].reports.remove({'_id' : ObjectId(str(report['_id']))})
                        kw_tasks = 0
                        break
                    for se in project['search_systems']:
                        se_name = se['se_name'].lower()
                        keyword = dict(keyword, **{'se_name' : se_name})
                        self.pool.append(keyword)
                        kw_tasks += 1

            if kw_tasks == 0:
                se_engine = {}
                for project_se in project['search_systems']:
                    search_hash = hashlib.md5(''.join([
                        project_se.get('se_country') or '',
                        project_se.get('se_name') or '',
                        project_se.get('se_domain') or '',
                        project_se.get('se_region') or '',
                    ]).encode('utf-8')).hexdigest()
                    se_engine[search_hash] = project_se
                    print(se_engine)

                reports_data_len = await self.dbs['positions'].reports_data.count({'report_id' : ObjectId(str(report['_id']))})
                print("\tPREPARE REPORT TO UPDATE {} - {} - {} results".format(','.join(project['domains']), report['_id'], reports_data_len))

                # get reports_data by limit
                i = 0
                _limit = 1000 # 1000 reports = 30MB
                # report_report = {}
                report_report = []
                project_statistic = {
                    'with_subdomains' : {'top_1_count' : 0, 'top_3_count' : 0, 'top_5_count' : 0, 'top_10_count' : 0, 'top_30_count' : 0, 'top_100_count' : 0, 'total_query_count' : 0},
                    'without_subdomains' : {'top_1_count' : 0, 'top_3_count' : 0, 'top_5_count' : 0, 'top_10_count' : 0, 'top_30_count' : 0, 'top_100_count' : 0, 'total_query_count' : 0}
                }
                while True:
                    _start = i * _limit
                    _end = _start + _limit
                    if _start >= reports_data_len : break
                    i += 1

                    print('\t\tget_reports_data {}:{}'.format(_start, _end))

                    reports_data = list(await self.dbs['positions'].reports_data.find({'report_id' : ObjectId(str(report['_id']))}, fields = {
                        'results' : 1,
                        'keyword_id' : 1,
                        'se_hash' : 1,
                    }, skip = _start, limit = _limit))

                    for report_data in reports_data:
                        report_data['keyword_id_str'] = str(report_data['keyword_id'])
                        find_with_domains = '-'
                        find_without_domains = '-'
                        project_statistic['with_subdomains']['total_query_count'] += 1
                        project_statistic['without_subdomains']['total_query_count'] += 1
                        for rd_v in report_data['results']:
                            if 'find_without_domains' in rd_v or 'find_with_domains' in rd_v:
                                find_with_domains = rd_v['position']
                                find_without_domains = rd_v['position']
                                if rd_v['position'] == 1:
                                    project_statistic['with_subdomains']['top_1_count'] += 1
                                    project_statistic['without_subdomains']['top_1_count'] += 1
                                if rd_v['position'] > 0 and rd_v['position'] <= 3 and rd_v['position'] != '-':
                                    project_statistic['with_subdomains']['top_3_count'] += 1
                                    project_statistic['with_subdomains']['top_5_count'] += 1
                                    project_statistic['with_subdomains']['top_10_count'] += 1
                                    project_statistic['with_subdomains']['top_30_count'] += 1
                                    project_statistic['with_subdomains']['top_100_count'] += 1
                                    project_statistic['without_subdomains']['top_3_count'] += 1
                                    project_statistic['without_subdomains']['top_5_count'] += 1
                                    project_statistic['without_subdomains']['top_10_count'] += 1
                                    project_statistic['without_subdomains']['top_30_count'] += 1
                                    project_statistic['without_subdomains']['top_100_count'] += 1
                                elif rd_v['position'] > 3 and rd_v['position'] <= 5 and rd_v['position'] != '-':
                                    project_statistic['with_subdomains']['top_5_count'] += 1
                                    project_statistic['with_subdomains']['top_10_count'] += 1
                                    project_statistic['with_subdomains']['top_30_count'] += 1
                                    project_statistic['with_subdomains']['top_100_count'] += 1
                                    project_statistic['without_subdomains']['top_5_count'] += 1
                                    project_statistic['without_subdomains']['top_10_count'] += 1
                                    project_statistic['without_subdomains']['top_30_count'] += 1
                                    project_statistic['without_subdomains']['top_100_count'] += 1
                                elif rd_v['position'] > 5 and rd_v['position'] <= 10 and rd_v['position'] != '-':
                                    project_statistic['with_subdomains']['top_10_count'] += 1
                                    project_statistic['with_subdomains']['top_30_count'] += 1
                                    project_statistic['with_subdomains']['top_100_count'] += 1
                                    project_statistic['without_subdomains']['top_10_count'] += 1
                                    project_statistic['without_subdomains']['top_30_count'] += 1
                                    project_statistic['without_subdomains']['top_100_count'] += 1
                                elif rd_v['position'] > 10 and rd_v['position'] <= 30 and rd_v['position'] != '-':
                                    project_statistic['with_subdomains']['top_30_count'] += 1
                                    project_statistic['with_subdomains']['top_100_count'] += 1
                                    project_statistic['without_subdomains']['top_30_count'] += 1
                                    project_statistic['without_subdomains']['top_100_count'] += 1
                                elif rd_v['position'] > 30 and rd_v['position'] <= 100 and rd_v['position'] != '-':
                                    project_statistic['with_subdomains']['top_100_count'] += 1
                                    project_statistic['without_subdomains']['top_100_count'] += 1
                                break
                            # if 'find_with_domains' in rd_v:
                            #   find_with_domains = rd_v['position']
                            # if 'find_without_domains' in rd_v:
                            #   find_without_domains = rd_v['position']
                        # if report_data['keyword_id_str'] not in report_report:
                        #   report_report[report_data['keyword_id_str']] = {}
                        report_report.append({
                            'report_id' : ObjectId(str(report['_id'])),
                            'keyword_id' : ObjectId(str(report_data['keyword_id'])),
                            'se_hash' : report_data['se_hash'],
                            'se_data' : se_engine[report_data['se_hash']],
                            'position' : {
                                'find_with_domains' : find_with_domains,
                                'find_without_domains' : find_without_domains,
                            }
                        })
                        # report_report[report_data['keyword_id_str']][report_data['se_hash']] = {
                        #   'find_with_domains' : find_with_domains,
                        #   'find_without_domains' : find_without_domains,
                        # }

                print('\t\tinsert report_report - count:', len(report_report))
                res = await self.dbs['positions'].reports.update({'_id' : ObjectId(str(report['_id']))}, {'$set' : {
                    'status' : 'success',
                    'date_finish' : datetime.utcnow(),
                    'se_engine' : se_engine,
                    # 'report' : report_report,
                    'project_statistic' : project_statistic,
                }}, safe = True)
                await self.dbs['positions'].reports_data_positions.insert(report_report)

            else:
                with await self.lock:
                    self.reports_in_work[str(report['_id'])] = 0
                    self.check_completed_tasks[str(report['_id'])] = kw_tasks

            project_data = {
                '_id' : project['_id'],
                'domains' : project['domains'],
            }

            search_systems = {}
            for se in project['search_systems']:
                search_systems[se['se_name'].lower()] = se

            use_subdomain_for_positions = False
            if 'use_subdomain_for_positions' in project:
                use_subdomain_for_positions = project['use_subdomain_for_positions']

            regionality = 'Ukraine'
            if 'regionality' in project:
                regionality = project['regionality']

            project_data['search_systems'] = search_systems
            project_data['use_subdomain_for_positions'] = use_subdomain_for_positions
            project_data['regionality'] = regionality

            self.projects_data[str(report['_id'])] = project_data

        # await asyncio.sleep(5, loop = self.main_loop)
        # asyncio.ensure_future(self.get_reports())

        if l_name not in self.events or self.events[l_name] == 5:
            self.events[l_name] = 1
            # print('COROUTINE {0}'.format(l_name))
        else:
            self.events[l_name] += 1
        await asyncio.sleep(self.sleep_after_loop, loop = self.main_loop)
        asyncio.ensure_future(self.get_reports())

    async def process_loop(self):
        l_name = 'process_loop'

        start = 0
        p_counter = 1
        while len(self.processes) < self.max_processes and len(self.pool) > 0:
            with await self.lock:
                tmp_pool = self.pool[start:self.max_tasks_per_process]
                del self.pool[start:self.max_tasks_per_process]
                counter = multiprocessing.Value('i', 0)
                process = multiprocessing.Process(target = self.init_process, args = (tmp_pool, counter,p_counter,))
                self.processes.append({'process' : process, 'counter' : counter, 'tasks' : tmp_pool})
                process.start()
                p_counter += 1
                print('---PROCESS START-----')

        for (i, p) in enumerate(self.processes):
            # print('CHECKING PROCESS ::: {0}'.format(p['process']))
            if p['process'].is_alive() is False:
                if p['counter'].value != len(p['tasks']):
                    print('\t ### RERUN ###')
                    with await self.lock:
                        self.pool += p['tasks']

                with await self.lock:
                    del self.processes[i]

                with await self.lock:
                    for task in p['tasks']:
                        try:
                            self.check_completed_tasks[str(task['report_id'])] -= 1
                        except:
                            self.check_completed_tasks[str(task['report_id'])] = 0

                    print('---check_completed_tasks---')
                    print(self.check_completed_tasks)
                    remove = []
                    for (k, v) in self.check_completed_tasks.items():
                        if v == 0 or v < 0:
                            remove.append(k)
                            print('---------START UPDATE CONNECTION-----------')
                            print('-------------UPDATE CONNECTED--------------')
                            se_engine = {}
                            print('---------------SS------------------')
                            print(self.projects_data[k]['search_systems'])
                            print('---------------SS------------------')
                            for (se_name,project_se) in self.projects_data[k]['search_systems'].items():
                                search_hash = hashlib.md5(''.join([
                                    project_se.get('se_country') or '',
                                    project_se.get('se_name') or '',
                                    project_se.get('se_domain') or '',
                                    project_se.get('se_region') or '',
                                ]).encode('utf-8')).hexdigest()
                                se_engine[search_hash] = project_se
                            print('----------STARTING UPDATE---------')
                            report = []
                            project_statistic = {
                                'with_subdomains' : {'top_1_count' : 0, 'top_3_count' : 0, 'top_5_count' : 0, 'top_10_count' : 0, 'top_30_count' : 0, 'top_100_count' : 0, 'total_query_count' : 0},
                                'without_subdomains' : {'top_1_count' : 0, 'top_3_count' : 0, 'top_5_count' : 0, 'top_10_count' : 0, 'top_30_count' : 0, 'top_100_count' : 0, 'total_query_count' : 0},
                            }
                            reports_data = await self.dbs['positions'].reports_data.find({'report_id' : ObjectId(k)})
                            for report_data in reports_data:
                                find_with_domains = '-'
                                find_without_domains = '-'
                                for rd_v in report_data['results']:
                                    # if 'find_with_domains' in rd_v:
                                    #   find_with_domains = rd_v['position']
                                    # if 'find_without_domains' in rd_v:
                                    #   find_with_domains = rd_v['position']
                                    if 'find_without_domains' in rd_v or 'find_with_domains' in rd_v:
                                        find_with_domains = rd_v['position']
                                        find_without_domains = rd_v['position']
                                        if rd_v['position'] == 1:
                                            project_statistic['with_subdomains']['top_1_count'] += 1
                                            project_statistic['without_subdomains']['top_1_count'] += 1
                                        if rd_v['position'] > 0 and rd_v['position'] <= 3 and rd_v['position'] != '-':
                                            project_statistic['with_subdomains']['top_3_count'] += 1
                                            project_statistic['with_subdomains']['top_5_count'] += 1
                                            project_statistic['with_subdomains']['top_10_count'] += 1
                                            project_statistic['with_subdomains']['top_30_count'] += 1
                                            project_statistic['with_subdomains']['top_100_count'] += 1
                                            project_statistic['without_subdomains']['top_3_count'] += 1
                                            project_statistic['without_subdomains']['top_5_count'] += 1
                                            project_statistic['without_subdomains']['top_10_count'] += 1
                                            project_statistic['without_subdomains']['top_30_count'] += 1
                                            project_statistic['without_subdomains']['top_100_count'] += 1
                                        elif rd_v['position'] > 3 and rd_v['position'] <= 5 and rd_v['position'] != '-':
                                            project_statistic['with_subdomains']['top_5_count'] += 1
                                            project_statistic['with_subdomains']['top_10_count'] += 1
                                            project_statistic['with_subdomains']['top_30_count'] += 1
                                            project_statistic['with_subdomains']['top_100_count'] += 1
                                            project_statistic['without_subdomains']['top_5_count'] += 1
                                            project_statistic['without_subdomains']['top_10_count'] += 1
                                            project_statistic['without_subdomains']['top_30_count'] += 1
                                            project_statistic['without_subdomains']['top_100_count'] += 1
                                        elif rd_v['position'] > 5 and rd_v['position'] <= 10 and rd_v['position'] != '-':
                                            project_statistic['with_subdomains']['top_10_count'] += 1
                                            project_statistic['with_subdomains']['top_30_count'] += 1
                                            project_statistic['with_subdomains']['top_100_count'] += 1
                                            project_statistic['without_subdomains']['top_10_count'] += 1
                                            project_statistic['without_subdomains']['top_30_count'] += 1
                                            project_statistic['without_subdomains']['top_100_count'] += 1
                                        elif rd_v['position'] > 10 and rd_v['position'] <= 30 and rd_v['position'] != '-':
                                            project_statistic['with_subdomains']['top_30_count'] += 1
                                            project_statistic['with_subdomains']['top_100_count'] += 1
                                            project_statistic['without_subdomains']['top_30_count'] += 1
                                            project_statistic['without_subdomains']['top_100_count'] += 1
                                        elif rd_v['position'] > 30 and rd_v['position'] <= 100 and rd_v['position'] != '-':
                                            project_statistic['with_subdomains']['top_100_count'] += 1
                                            project_statistic['without_subdomains']['top_100_count'] += 1
                                        break
                                project_statistic['with_subdomains']['total_query_count'] += 1
                                project_statistic['without_subdomains']['total_query_count'] += 1
                                # if str(report_data['keyword_id']) not in report:
                                #   report[str(report_data['keyword_id'])] = {}
                                # report[str(report_data['keyword_id'])][report_data['se_hash']] = {
                                #   'find_with_domains' : find_with_domains,
                                #   'find_without_domains' : find_without_domains
                                # }
                                report.append({
                                    'report_id' : ObjectId(str(report_data['report_id'])),
                                    'keyword_id' : ObjectId(str(report_data['keyword_id'])),
                                    'se_hash' : report_data['se_hash'],
                                    'se_data' : se_engine[report_data['se_hash']],
                                    'position' : {
                                        'find_with_domains' : find_with_domains,
                                        'find_without_domains' : find_without_domains,
                                    }
                                })

                            res = await self.dbs['positions'].reports.update(
                                {'_id' : ObjectId(k)},
                                {
                                    '$set' : {
                                        'status' : 'success',
                                        'date_finish' : datetime.utcnow(),
                                        'se_engine' : se_engine,
                                        'project_statistic' : project_statistic,
                                        # 'report' : report
                                    }
                                }, safe = True)
                            res = await self.dbs['positions'].reports_data_positions.insert(report)
                            print('-----------UPDATE DONE------------')
                            print('FINISH REPORT {0}'.format(','.join(self.projects_data[k]['domains'])))
                        else:
                            print(', '.join(self.projects_data[k]['domains']) + ' : left ' + str(v))

                    for k in remove:
                        try:
                            del self.reports_in_work[k]
                        except:
                            pass
                        try:
                            del self.check_completed_tasks[k]
                        except:
                            pass

        if l_name not in self.events or self.events[l_name] == 5:
            self.events[l_name] = 1
            # print('COROUTINE {0}'.format(l_name))
            # print('PROCESESS: {0}'.format(len(self.processes)))
            # print('POOL SIZE: {0}'.format(len(self.pool)))
        else:
            self.events[l_name] += 1
        await asyncio.sleep(self.sleep_after_loop, loop = self.main_loop)
        asyncio.ensure_future(self.process_loop())

    async def start_process(self, pool, counter, loop, p_counter):
        tasks_list = list(self.chunks(pool, self.max_task_per_worker))
        workers = []
        data = []
        for (worker_num, task) in enumerate(tasks_list):
            workers.append(self.start_se_process(task, loop, worker_num, p_counter))
        try:
            data = await asyncio.gather(*workers, loop = loop)
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            tb = traceback.format_exc()
            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~start_process~~~~~~~~~~~~~")
            print(exc_type, fname, exc_tb.tb_lineno, tb)
            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~start_process~~~~~~~~~~~~~")

        #здесь результаты вида [[results:...], [results:...], [results:...], [results:...]]
        to_insert = []
        insertCompetitors = []
        for d in data:
            try:
                for reportDataItem in d:
                    top10Results = reportDataItem['results'][:10]
                    for topItem in top10Results:
                        finalUrl = '{}://{}{}'.format(topItem['scheme'], topItem['toplevel_domain'], topItem['url'])
                        finalUrlHash = hashlib.md5(finalUrl.encode('utf-8')).hexdigest()
                        checkExistingCompetitor = await self.dbs['positions'].competitors_links.find_one({'url_hash' : finalUrlHash}, {'_id' : 1})
                        if checkExistingCompetitor == {} or checkExistingCompetitor is None:
                            insertCompetitors.append({
                                'se_name' : reportDataItem['se_name'],
                                'se_domain' : reportDataItem['se_domain'],
                                'position' : topItem['position'],
                                'url' : finalUrl,
                                'url_hash' : finalUrlHash,
                                'domain' : topItem['toplevel_domain'],
                                'date_add' : datetime.utcnow(),
                            })
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                tb = traceback.format_exc()
                print('~~~~~~~')
                print(exc_type, fname, exc_tb.tb_lineno, tb)
                print('~~~~~~~')
                pass
            to_insert += d
        counter.value = len(to_insert)
        print('----PRE-FINISH PROCESS INSERT----')
        if insertCompetitors != []:
            await self.dbs['positions'].competitors_links.insert(insertCompetitors, safe = True)
        if to_insert != []:
            await self.dbs['positions'].reports_data.insert(to_insert, safe = True)
        print('----PROCESS FINISHED---')

    async def login(self, loop, region = 'ua', acc = {}):
        region = self.searchRegion
        self.driver.get('https://www.google.com/preferences')
        WebDriverWait(self.driver, 15).until(lambda driver: self.driver.execute_script('return document.readyState') == 'complete')
        # try:
        #   wait = WebDriverWait(self.driver, 10)
        #   wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(@href, '/preferences')]")))
        # except Exception as e:
        #   print('~~~~~~~~~~~~~~~~~~~~~')
        #   print('Failed wait for preferences link reloading page')
        #   print('~~~~~~~~~~~~~~~~~~~~~')
        #   return False

        # self.driver.find_element(By.XPATH, "//a[contains(@href, '/preferences')]").click();

        # try:
        #   wait = WebDriverWait(self.driver, 10)
        #   wait.until(EC.element_to_be_clickable((By.XPATH, "//span[@id='fsett']/a[contains(@href, '/preferences')]")))
        # except Exception as e:
        #   self.save_driver('preferences_2_error')
        #   print('~~~~~~~~~~~~~~~~~~~~~')
        #   print('Failed wait for preferences #2 link reloading page')
        #   print('~~~~~~~~~~~~~~~~~~~~~')
        #   return False

        # self.driver.find_element(By.XPATH, "//span[@id='fsett']/a[contains(@href, '/preferences')]").click()

        # WebDriverWait(self.driver, 15).until(lambda driver: self.driver.execute_script('return document.readyState') == 'complete')
        try:
            wait = WebDriverWait(self.driver, 10)
            # wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@id='regionanchormore']")))
            wait.until(EC.element_to_be_clickable((By.ID, 'regionanchormore')))
        except Exception as e:
            print('~~~~~~~~~~~~~~~~~~~~~')
            print('Failed waiting for more regions link restarting')
            print('~~~~~~~~~~~~~~~~~~~~~')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            tb = traceback.format_exc()
            print(exc_type, fname, exc_tb.tb_lineno, tb)
            return False

        if region != 'com':
            regionanchormore = self.driver.find_element(By.ID, 'regionanchormore')
            self.driver.execute_script("arguments[0].click();", regionanchormore)
            self.driver.execute_script("document.getElementById('regionother').style.display = 'block';")
            await asyncio.sleep(1, loop = loop)

            # more_regions_link.click()
            # more_regions_link.send_keys("\n")
            # self.driver.execute_script("return arguments[0].click();", more_regions_link)

            try:
                print("wait started")
                wait = WebDriverWait(self.driver, 3)
                if region == 'by':
                    wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@id='regiont{}']".format(region.upper()))))
                else:
                    wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@id='regiono{}']".format(region.upper()))))
            except Exception as e:
                self.save_driver('test_radio_not_present')
                print('~~~~~~~~~~~~~~~~~~~~~')
                print('Failed waiting for regions div (radio button)')
                print("//div[@id='regiono{}']".format(region.upper()))
                print('~~~~~~~~~~~~~~~~~~~~~')
                return False
            if region == 'by':
                radio = self.driver.find_element(By.XPATH, "//div[@id='regiont{}']".format(region.upper()))
            else:
                radio = self.driver.find_element(By.XPATH, "//div[@id='regiono{}']".format(region.upper()))
            self.driver.execute_script("return arguments[0].click();", radio)
            radioRegion = self.driver.find_element(By.XPATH, "//div[@data-value='{}']".format(region.upper()))
            self.driver.execute_script("arguments[0].click();", radioRegion)
        # self.driver.execute_script("document.querySelectorAll('{0}')[0].removeAttribute('type')".format('[name="num"]'))
        # inp = self.driver.find_element(By.XPATH, '//input[@name="num"]')
        # current_num_value = inp.get_attribute('value')
        # inp.clear()
        # inp.send_keys(100)

        try:
            wait = WebDriverWait(self.driver, 10)
            wait.until(EC.element_to_be_clickable((By.XPATH, '//div[@id="form-buttons"]/div[contains(@class, "action")]')))
        except:
            print('~~~~~~~~~~~~~~~~~~~~~')
            print('Failed waiting for save button to be clickable')
            print('~~~~~~~~~~~~~~~~~~~~~')
            return False
        
        # btn = self.driver.find_element(By.XPATH, '//div[@id="form-buttons"]/div[contains(@class, "action")]')
        # btn.click()
        # self.driver.execute_script("arguments[0].click();", btn)
        self.driver.find_element(By.XPATH, '//form').submit()

        try:
            wait = WebDriverWait(self.driver, 10)
            wait.until(EC.alert_is_present())
            alert = self.driver.switch_to.alert
            alert.accept()
            print('Alert accepted')
        except Exception as e:
            return False

        WebDriverWait(self.driver, 10).until(lambda driver: self.driver.execute_script('return document.readyState') == 'complete')
        print('-------------')
        print('logged in done')
        print('-------------')
        return True

    def page_has_loaded(self):
        print("Checking if {} page is loaded.".format(self.driver.current_url))
        page_state = self.driver.execute_script('return document.readyState;')
        return page_state == 'complete'

    def save_driver(self, action):
#       self.driver.save_screenshot('./google/{0}.png'.format(action))
        with open('./google/{0}.html'.format(action), 'w') as f:
            f.write(self.driver.page_source)
        return True

    async def query(self, *args, **kwargs):
        url = kwargs['url']
        data = kwargs.get('data')
        loop = kwargs.get('loop')
        page = kwargs.get('page', 1)
        try:
            if 'google' in url:
                await asyncio.sleep(3, loop = loop)
                try:
                    q = self.driver.find_element(By.XPATH, "//form[@role='search']//*[@name='q']")
                    q.click()
                    q.clear()
                    q.send_keys(data['keyword'])
                    q.send_keys(Keys.ENTER)
                    await asyncio.sleep(3, loop = loop)
                except:
                    print('Problem with query input')
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    tb = traceback.format_exc()
                    print('~~~~~~~')
                    print(exc_type, fname, exc_tb.tb_lineno, tb)
                    print('~~~~~~~')
                    return False

                currentURL = self.driver.current_url
                await self.wait_for_page_loaded(loop)
                if 'num=100' not in currentURL and 'sorry/index' not in currentURL:
                    self.driver.get(self.driver.current_url+'&num=100')
                await self.wait_between(1, 3, loop = loop)
                return self.driver.page_source
            else:
                self.driver.get(url)
                return self.driver.page_source
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            tb = traceback.format_exc()
            print('~~~~~~~~~~~~query~~~~~~~~~~~~~ - url - {}'.format(kwargs['url']))
            print(exc_type, fname, exc_tb.tb_lineno, tb)
            print('~~~~~~~~~~~~query~~~~~~~~~~~~~')
            await asyncio.sleep(1)
            return False

    async def fetch_data(self, url, se_name, loop, project, task_data, proxy, proxy_md5):
        try:
            search_system = project['search_systems'][se_name]
            results = []
            page_num = 1
            bad_content_retry = 0
            skipped = False
            check = 0
            while True:
                skipped = False
#               response = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5)
                if se_name == 'google':
                    currentCookies = self.driver.get_cookies()
                    response = await self.query(url = url, data = task_data, loop = loop)
                    if response is False:
                        print('~~~~ Broken query input ~~~~')
                        proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                        proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                        await self.get_driver(loop, proxy, proxy_md5)
                        await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
                        continue
                    response = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
                    try:
                        tmp_results = self.parse_response(se_name, loop, task_data)
                        if len(tmp_results) == 0 and check <= 5:
                            print('EMPTY RESPONSE, TRYING TO RECHECK')
                            self.save_driver('empty_response_trying_to_recheck')
                            check += 1
                            continue
                        # print("RESULTS google: {}".format(len(tmp_results)))
                    except:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        tb = traceback.format_exc()
                        print('~~~~~~~~~~~~parse response shit~~~~~~~~~~~~~')
                        print(exc_type, fname, exc_tb.tb_lineno, tb)
                        print('~~~~~~~~~~~~parse response shit~~~~~~~~~~~~~')
                        proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                        proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                        await self.get_driver(loop, proxy, proxy_md5)
                        continue
                elif se_name == 'yandex':
                    url += '&numdoc=50'
                    response = await self.query(url = url, data = task_data, loop = loop)
                    response = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
                    tmp_results = self.parse_response(se_name, loop, task_data)
                    url += '&p=2'
                    response = await self.query(url = url, data = task_data, loop = loop)
                    response = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
                    tmp_results += self.parse_response(se_name, loop, task_data)
                    print("RESULTS yandex: {}".format(len(tmp_results)))
                try:
                    check_empty_response = self.driver.find_elements(By.XPATH, '//div[@id="search"]/*')
                except:
                    print("~~~~EMPTY RESPONSE + BROKEN HTML~~~~")
                    continue
                if len(tmp_results ) == 0 and '/sorry/index?' in self.driver.current_url:
                    print("~~~~EMPTY RESPONSE + CAPTCHA~~~~")
                    continue
                if len(tmp_results) == 0:
                    if self.driver.current_url == 'https://www.google.com.ua/' or self.driver.current_url == 'https://www.google.com.ua/?safe=images':
                        continue
#                if len(tmp_results) == 0 and (('<div id="search"></div>' not in self.driver.page_source and ('Error 403 (Forbidden)!!1' in self.driver.page_source or '<html xmlns="http://www.w3.org/1999/xhtml"><head></head><body></body></html>' in self.driver.page_source)) or ('class="rc"' not in self.driver.page_source and se_name == 'google' and '<div id="search"></div>' not in self.driver.page_source and self.driver.current_url != 'https://www.google.com.ua/?safe=images') and 'Результатов: примерно 0' not in self.driver.page_source):
                if len(tmp_results) == 0 and ('<div id="search"></div>' not in self.driver.page_source and ('Error 403 (Forbidden)!!1' in self.driver.page_source or '<html xmlns="http://www.w3.org/1999/xhtml"><head></head><body></body></html>' in self.driver.page_source)):
                    print('~~~~FORBIDDEN RESTART DRIVER~~~~')
                    if se_name == 'google':
                        proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                        proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                        await self.get_driver(loop, proxy, proxy_md5)
                    else:
                        await self.get_driver_yandex(loop, proxy, proxy_md5)
                    print('~~~~CHECK RECAPTCHA AFTER FORBIDDEN~~~~')
                    print(self.driver)
                    print('~~~~CHECK RECAPTCHA AFTER FORBIDDEN~~~~')
                    response = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
                    skipped = True
                    continue
#                if ((len(tmp_results) == 0 and len(check_empty_response) != 0) or (' id="search"' not in self.driver.page_source and ' name="q"' in self.driver.page_source)):
#                    print("~~~~EMPTY RESPONSE~~~~")
#                    continue
                #if len(tmp_results) == 0 and skipped is False:
                #    print('~~~~SOURCE~~~~')
                #    print(task_data)
                #    self.save_driver('test_empty_response')
                #    print('~~~~SOURCE~~~~')
                # print('\t\t\t\t\t\t\t\t tmp_results : {0} in {1}'.format(len(tmp_results), se_name))
                results += tmp_results

                if se_name == 'google':
                    break
                break

            results = self.prepare_results(results, project)
            return results
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            tb = traceback.format_exc()
            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~BREAD~~~~~~~~~~~~~")
            print(exc_type, fname, exc_tb.tb_lineno, tb)
            self.save_driver('BREAD')
            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~BREAD~~~~~~~~~~~~~")
            await asyncio.sleep(1, loop = loop)
            return False

    async def get_driver_yandex(self, loop, proxy, proxy_md5):
        chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors.
        # chrome_options.add_argument('--proxy-server=%s' % proxy)
        chrome_options.add_argument('--lang=ru')
        chrome_options.add_argument('--incognito')
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument('--disable-infobars')
        chrome_options.add_argument('window-size=1920x5000')
        # chrome_options.add_argument('user-data-dir=./user_profile/{}'.format(proxy_md5))
        prefs = {"profile.default_content_setting_values.geolocation" : 2}
        chrome_options.add_experimental_option("prefs",prefs)
        d_cap = DesiredCapabilities.CHROME
        d_cap['loggingPrefs'] = {'browser' : 'ALL'}
        d_cap['pageLoadStrategy'] = "none"

        options = {
            'proxy': {
                'http': 'http://sqduser:GyOjj4m1iaQbS@{}'.format(proxy),
                'https': 'https://sqduser:GyOjj4m1iaQbS@{}'.format(proxy),
                'no_proxy': 'localhost,127.0.0.1'
            }
        }
        # self.driver = webdriver.Chrome('/root/chromedriver_31_07_2021', chrome_options=chrome_options, desired_capabilities = d_cap)
        # self.driver = webdriver.Chrome('/root/chromedriver_02_11_2021', chrome_options=chrome_options, desired_capabilities = d_cap)
        self.driver = webdriver.Chrome('/root/chromedriver_30_06_22', chrome_options=chrome_options, desired_capabilities = d_cap)
        self.driver.implicitly_wait(10)
        self.driver.set_page_load_timeout(10)
        self.driver.set_script_timeout(10)
        self.driver.search_engine = 'yandex'
        #self.driver = webdriver.Chrome('/usr/bin/chromedriver', chrome_options=chrome_options)
        self.driver.get('https://yandex.ru')
        self.driver.delete_all_cookies()

        return True
        # print('Open tab')
        # self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 't')
        # print('Navigate tab')
        # self.driver.get('https://www.google.com.ua')

    def proxies(self, username, password, endpoint, port):
        manifest_json = """
        {
            "version": "1.0.0",
            "manifest_version": 2,
            "name": "Proxies",
            "permissions": [
                "proxy",
                "tabs",
                "unlimitedStorage",
                "storage",
                "<all_urls>",
                "webRequest",
                "webRequestBlocking"
            ],
            "background": {
                "scripts": ["background.js"]
            },
            "minimum_chrome_version":"22.0.0"
        }
        """

        background_js = """
        var config = {
                mode: "fixed_servers",
                rules: {
                  singleProxy: {
                    scheme: "http",
                    host: "%s",
                    port: parseInt(%s)
                  },
                  bypassList: ["localhost"]
                }
              };
        chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});
        function callbackFn(details) {
            return {
                authCredentials: {
                    username: "%s",
                    password: "%s"
                }
            };
        }
        chrome.webRequest.onAuthRequired.addListener(
                    callbackFn,
                    {urls: ["<all_urls>"]},
                    ['blocking']
        );
        """ % (endpoint, port, username, password)

        extension = 'proxies_extension.zip'

        with zipfile.ZipFile(extension, 'w') as zp:
            zp.writestr("manifest.json", manifest_json)
            zp.writestr("background.js", background_js)

        return extension

    async def get_driver(self, loop, proxy, proxy_md5):
        while True:
            try:
                self.driver.delete_all_cookies()
            except:
                pass
            try:
                self.driver.close()
            except:
                pass
            try:
                self.driver.quit()
            except:
                pass

            binary = '/Applications/Tor Browser.app/Contents/MacOS/firefox'
            # the location of firefox package inside Tor
            if os.path.exists(binary) is False:
                raise ValueError("The binary path to Tor firefox does not exist.")
            firefox_binary = FirefoxBinary(binary)
            options = Options()
            options.headless = True
            options.binary_location = binary

            # self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(),options=options)
            self.driver = webdriver.Firefox(executable_path = '/usr/local/bin/geckodriver', options=options)
            self.driver.set_window_size(1920, 2000)

            # click on connect to connect the tor browser to the remote nodes
            self.driver.find_element(By.XPATH, '//*[@id="connectButton"]').click()
            while True:
                if 'torconnect' in self.driver.current_url:
                    await asyncio.sleep(1, loop = loop)
                    continue
                break

            # self.driver.implicitly_wait(10)
            # self.driver.set_page_load_timeout(10)
            # self.driver.set_script_timeout(10)
            self.driver.search_engine = 'google'
            self.driver.get('https://www.google.com/')
            await asyncio.sleep(1, loop = loop)
            if 'policies.google.com/technologies/cookies' in self.driver.page_source:
                self.driver.find_element(By.XPATH, '//div[@aria-modal="true"]/div[3]/span/div/div/div/div[3]/div[1]/button[2]').click()
                await asyncio.sleep(1, loop = loop)
            elif 'Before you continue to Google' in self.driver.page_source:
                cookiebutton = self.driver.find_element(By.XPATH, '//*[text() = "Accept all"]')
                cookiebutton.click()
            if 'google' not in self.driver.page_source and 'Google' not in self.driver.page_source:
                proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                print('~~~~~~~~~~~~~~~~Seems like proxy error~~~~~~~~~~~~~~~~')
                print(self.driver.page_source)
                await asyncio.sleep(1, loop = loop)
                continue
            _check = await self.login(loop = loop)
            await self.check_recaptcha_with_audio(proxy, proxy_md5, True)
            if _check is True:
                break
        return True

    async def get_driver_old(self, loop, proxy, proxy_md5):
        while True:
            try:
                self.driver.delete_all_cookies()
            except:
                pass
            try:
                self.driver.close()
            except:
                pass
            try:
                self.driver.quit()
            except:
                pass


            proxyInfo = proxy.split('@')
            proxyUser, proxyPass = proxyInfo[0].split(':')
            proxyHost, proxyPort = proxyInfo[1].split(':')
            proxyDict = {
                'username' : proxyUser,
                'password' : proxyPass,
                'host' : proxyHost,
                'port' : proxyPort,
            }
            
            chrome_options = webdriver.ChromeOptions()
            # chrome_options.add_argument('--headless')
            chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors.
            # chrome_options.add_argument('--proxy-server={}'.format('http://sqduser:GyOjj4m1iaQbS@{}'.format(proxy)))
            chrome_options.add_argument('--lang=ru')
            # chrome_options.add_argument('--incognito')
            chrome_options.add_argument('--disable-infobars')
            chrome_options.add_argument('window-size=1920x1080')
            chrome_options.add_argument('--allow-insecure-localhost')
            chrome_options.add_argument('--allow-running-insecure-content')
            chrome_options.add_argument('--enable-javascript')
            chrome_options.add_argument('--ignore-certificate-errors')
            chrome_options.set_capability("acceptInsecureCerts", True)
            chrome_options.set_capability("acceptSslCerts", True)
            proxyInfo = proxy.split('@')
            proxyUser, proxyPass = proxyInfo[0].split(':')
            proxyHost, proxyPort = proxyInfo[1].split(':')
            proxies_extension = self.proxies(proxyUser, proxyPass, proxyHost, proxyPort)

            chrome_options.add_extension(proxies_extension)
            # chrome_options.add_extension('./plugin.zip')
            # chrome_options.add_argument("--headless=new")

            # chrome_options.add_argument('user-data-dir=./user_profile/{}'.format(proxy_md5))
            d_cap = DesiredCapabilities.CHROME.copy()
            d_cap['loggingPrefs'] = {'browser' : 'ALL'}
            d_cap['pageLoadStrategy'] = "none"
            d_cap['acceptInsecureCerts'] = True
            

            print("STARTING DRIVER : {}".format(proxy))
            
            self.driver = webdriver.Chrome('/usr/local/bin/chromedriver', chrome_options=chrome_options, desired_capabilities = d_cap)

            self.driver.switch_to_window(self.driver.current_window_handle)

            self.driver.implicitly_wait(10)
            self.driver.set_page_load_timeout(10)
            self.driver.set_script_timeout(10)
            self.driver.search_engine = 'google'
            self.driver.get('https://www.google.com/')
            await asyncio.sleep(1, loop = loop)
            if 'policies.google.com/technologies/cookies' in self.driver.page_source:
                self.driver.find_element(By.XPATH, '//div[@aria-modal="true"]/div[3]/span/div/div/div/div[3]/div[1]/button[2]').click()
                await asyncio.sleep(1)
            elif 'Before you continue to Google' in self.driver.page_source:
                cookiebutton = self.driver.find_element(By.XPATH, '//*[text() = "Accept all"]')
                cookiebutton.click()
            if 'google' not in self.driver.page_source and 'Google' not in self.driver.page_source:
                proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                print('~~~~~~~~~~~~~~~~Seems like proxy error~~~~~~~~~~~~~~~~')
                print(self.driver.page_source)
                await asyncio.sleep(1, loop = loop)
                continue
            _check = await self.login(loop)
            if _check is True:
                break
        return True

    async def solve_image_captcha(self, loop, proxy, proxy_md5):
        TwoCaptchaConfig = {
            'server': '2captcha.com',
            'apiKey': 'eaf1fdec934c2b6a274c4ebfef4290d3',
            'defaultTimeout': 120,
            'recaptchaTimeout': 600,
            'pollingInterval': 10
        }
        await asyncio.sleep(1, loop = loop)
        solver = TwoCaptcha(**TwoCaptchaConfig)
        element = self.driver.find_element(By.XPATH, '//img[contains(@src, "sorry")]')
        location = element.location
        size = element.size
        png = self.driver.get_screenshot_as_png() # saves screenshot of entire page
        im = Image.open(BytesIO(png)) # uses PIL library to open image in memory

        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        im = im.crop((left*2, top*2, right*2, bottom*2)) # defines crop points
        savePath = './google/{}_captcha.png'.format(proxy_md5)
        im.save(savePath) # saves new cropped image
        print('SOLVING IMAGE CAPTCHA : {}'.format(savePath))
        result = solver.normal(savePath)
        print('SOLVED IMAGE CAPTCHA')
        token = result['code']
        captchaInput = self.driver.find_element(By.XPATH, '//input[@id="captcha"]')
        print(token)
        await asyncio.sleep(1, loop = loop)
        captchaInput.send_keys(token)
        await asyncio.sleep(.5, loop = loop)
        captchaInput.send_keys(Keys.ENTER)

    async def check_recaptcha_2captcha(self, loop, proxy, proxy_md5, isLogin = False, cookies = False):
        non_solvable_count = 0
        while True:
            if non_solvable_count > 3:
                print(' --- seems like ip is banned ---')
                proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                await self.get_driver(loop, proxy, proxy_md5)
                non_solvable_count = 0
                continue
            if self.driver.current_url.endswith('sorry/index'):
                print(' --- SORRY/INDEX OCCURED RESTARTING DRIVER ---')
                # proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                # proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                await self.get_driver(loop, proxy, proxy_md5)
                continue
            if '/sorry/index' in self.driver.current_url:
                TwoCaptchaConfig = {
                    'server': '2captcha.com',
                    'apiKey': 'eaf1fdec934c2b6a274c4ebfef4290d3',
                    'defaultTimeout': 120,
                    'recaptchaTimeout': 600,
                    'pollingInterval': 10
                }
                solver = TwoCaptcha(**TwoCaptchaConfig)


                print("\t\t\t\t\t\t\t HOLY CAPTCHA")
                if 'id="recaptcha"' in self.driver.page_source:
                    wrapper = self.driver.find_element(By.XPATH, '//div[@id="recaptcha"]')
                    dataSitekey = wrapper.get_attribute('data-sitekey')
                    dataS = wrapper.get_attribute('data-s')
                    print('SOLVING RECAPTCHA')
                    try:
                        result = solver.recaptcha(
                            sitekey = dataSitekey,
                            url = self.driver.current_url,
                            datas = dataS,
                            method = 'userrecaptcha',
                            version = 'v2')
                        if 'code' in result:
                            token = result['code']
                            print('SUBMITTING : {}'.format(token))
                            self.driver.execute_script('document.getElementById("g-recaptcha-response").innerHTML="{}";'.format(token))
                            self.driver.execute_script('submitCallback("{}")'.format(token))
                    except:
                        non_solvable_count += 3
                        continue
                    await asyncio.sleep(1, loop = loop)
                    print(self.driver.current_url)
                    if 'sorry/index' not in self.driver.current_url:
                        break
                    await asyncio.sleep(.1, loop = loop)
                    continue
                elif '/sorry/image?' in self.driver.page_source:
                    print(' --- IMAGE CAPTCHA !!! --- ')
                    element = self.driver.find_element(By.XPATH, '//img')
                    location = element.location
                    size = element.size
                    png = self.driver.get_screenshot_as_png() # saves screenshot of entire page
                    im = Image.open(BytesIO(png)) # uses PIL library to open image in memory

                    left = location['x']
                    top = location['y']
                    right = location['x'] + size['width']
                    bottom = location['y'] + size['height']

                    im = im.crop((left, top, right, bottom)) # defines crop points
                    savePath = './google/{}_captcha.png'.format(proxy_md5)
                    im.save(savePath) # saves new cropped image
                    imgItem = self.driver.find_element(By.XPATH, '//img')
                    print('SOLVING IMAGE CAPTCHA')
                    result = solver.normal(savePath)
                    print('SOLVED IMAGE CAPTCHA')
                    token = result['code']
                    captchaInput = self.driver.find_element(By.XPATH, '//input[@id="captcha"]')
                    captchaInput.send_keys(token)
                    captchaSubmit = self.driver.find_element(By.XPATH, '//input[@type="submit"]')
                    captchaSubmit.click()
                    await asyncio.sleep(1, loop = loop)
                    print(self.driver.current_url)
                    continue
                else:
                    proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                    proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                    await self.get_driver(loop, proxy, proxy_md5)
                    continue
            break
        return self.driver.page_source
    async def check_recaptcha_v2(self, loop, proxy, proxy_md5, isLogin = False, cookies = False):
        non_solvable_count = 0
        while True:
            if non_solvable_count > 3:
                print(' --- seems like ip is banned ---')
                proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                await self.get_driver(loop, proxy, proxy_md5)
                non_solvable_count = 0
                continue
            if self.driver.current_url.endswith('sorry/index'):
                print(' --- SORRY/INDEX OCCURED ---')
                proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                await self.get_driver(loop, proxy, proxy_md5)
                continue
            if '/sorry/index' in self.driver.current_url:
                print("\t\t\t\t\t\t\t HOLY CAPTCHA")
                if 'id="recaptcha"' in self.driver.page_source:
                    print(' --- log --- ')
                    for log in self.driver.get_log('browser'): print(log)
                    print(' --- log --- ')
                    try:
                        wait = WebDriverWait(self.driver, 15)
                        wait.until(EC.presence_of_element_located((By.XPATH, '//*[contains(@class,"antigate_solver")]')))
                    except:
                        print('unable_to_solve_not_initialized')
                        await self.get_driver(loop, proxy, proxy_md5)
                        continue
                    try:
                        wait = WebDriverWait(self.driver, 120)
                        wait.until(EC.presence_of_element_located((By.XPATH, '//*[contains(@class,"antigate_solver")][contains(@class, "solved")]')))
                    except:
                        print('unable_to_solve')
                        non_solvable_count += 1
                        await self.get_driver(loop, proxy, proxy_md5)
                        continue
                    print('SOLVED SOLVED SOLVED SOLVED SOLVED')
                    await asyncio.sleep(.1, loop = loop)
                    submitForm = self.driver.find_element(By.XPATH, '//form')
                    submitForm.submit()
                    await asyncio.sleep(.1, loop = loop)
                    continue
                else:
                    proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                    proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                    await self.get_driver(loop, proxy, proxy_md5)
                    continue
            break
        return self.driver.page_source

    async def wait_for_page_loaded(self, loop):
        while True:
            if self.page_has_loaded() is False:
                await asyncio.sleep(1, loop = loop)
            else:
                break

    def page_has_loaded(self):
        # print("Checking if {} page is loaded.".format(self.driver.current_url))
        page_state = self.driver.execute_script('return document.readyState;')
        return page_state == 'complete'

    async def check_recaptcha_with_audio(self, loop, proxy, proxy_md5, isLogin = False):
        _continue = 0
        isYandex = False
        while True:
            await self.wait_for_page_loaded(loop)
            try:
                captcha = False
                if self.driver.current_url.endswith('sorry/index'):
                    proxy = self.get_available_proxy('se_positions')
                    proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                    await self.get_driver(loop, proxy, proxy_md5)
                    continue
                if '/sorry/index?' in self.driver.current_url:
                    captcha = True
                if captcha is not True:
                    try:
                        body = self.driver.find_element(By.XPATH, '//body')
                    except:
                        _continue = 100
                        raise Exception('Wrong response')
                    onload = body.get_attribute('onload')
                    if onload is not None and 'captcha' in onload:
                        captcha = True
                if captcha is True:
                    if 'sorry/image' in self.driver.page_source:
                        try:
                            await self.solve_image_captcha(loop, proxy, proxy_md5)
                        except:
                            continue
                        continue
                    else:
                        print("\t\t\t\t\t\t\t HOLY CAPTCHA | CONTINUE: {0}".format(_continue))
                        print(self.driver.current_url)
                        audioCaptchaPath = os.path.abspath(os.getcwd())+'/audio_captcha'
                        try:
                            frames = self.driver.find_elements_by_tag_name("iframe")
                            self.driver.switch_to.frame(frames[0])
                            await asyncio.sleep(randint(2, 4), loop = loop)

                            self.driver.find_element_by_class_name("recaptcha-checkbox-border").click()
                            self.driver.switch_to.default_content()
                            frames = self.driver.find_element_by_xpath("/html/body/div[2]/div[4]").find_elements_by_tag_name("iframe")
                            await asyncio.sleep(randint(2, 4), loop = loop)
                            self.driver.switch_to.default_content()
                            frames = self.driver.find_elements_by_tag_name("iframe")
                            self.driver.switch_to.frame(frames[-1])
                            self.driver.find_element_by_id("recaptcha-audio-button").click()
                            self.driver.switch_to.default_content()
                            frames = self.driver.find_elements_by_tag_name("iframe")
                            self.driver.switch_to.frame(frames[-1])
                            await asyncio.sleep(randint(2, 4), loop = loop)
                            try:
                                self.driver.find_element_by_xpath("/html/body/div/div/div[3]/div/button").click()
                            except:
                                self.driver.find_element_by_class_name("rc-button-reload").click()
                                continue

                            src = self.driver.find_element_by_id("audio-source").get_attribute("src")
                            urlrequest.urlretrieve(src, audioCaptchaPath+"/{}.mp3".format(proxy_md5))
                            sound = pydub.AudioSegment.from_mp3(audioCaptchaPath+"/{}.mp3".format(proxy_md5)).export(audioCaptchaPath+"/{}.wav".format(proxy_md5), format="wav")
                            recognizer = Recognizer()
                            recaptcha_audio = AudioFile(audioCaptchaPath+"/{}.wav".format(proxy_md5))
                            with recaptcha_audio as source:
                                audio = recognizer.record(source)
                            text = recognizer.recognize_google(audio, language="en-US")

                            print('Captcha solved: {}'.format(text))

                            inputfield = self.driver.find_element_by_id("audio-response")
                            inputfield.send_keys(text.lower())
                            inputfield.send_keys(Keys.ENTER)

                            await asyncio.sleep(1, loop = loop)
                        except:
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                            tb = traceback.format_exc()
                            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~BREAD~~~~~~~~~~~~~")
                            print(exc_type, fname, exc_tb.tb_lineno, tb)
                            self.save_driver('BREAD')
                            print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~BREAD~~~~~~~~~~~~~")
                            await asyncio.sleep(1000, loop = loop)
                            proxy = self.get_available_proxy('se_positions')
                            proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                            await self.get_driver(loop, proxy, proxy_md5)
                            continue

                    print('CURRENT URL: {}'.format(self.driver.current_url))
                    if self.driver.current_url.endswith('sorry/index'):
                        proxy = self.get_available_proxy('se_positions')
                        proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                        await self.get_driver(loop, proxy, proxy_md5)
                        continue

                    captcha = False
                    if '/sorry/index?' in self.driver.current_url:
                        captcha = True
                    if captcha is not True:
                        body = self.driver.find_element(By.XPATH, '//body')
                        onload = body.get_attribute('onload')
                        if onload is not None and 'captcha' in onload:
                            captcha = True
                    if captcha is True:
                        raise Exception('Another captcha')
                        continue
                    '''
                    content = self.driver.page_source
                    try:
                        wait = WebDriverWait(self.driver, 10)
                        wait.until(EC.presence_of_element_located((By.XPATH, '//textarea[@name="g-recaptcha-response"]')))
                    except:
                        print('~~~~~~~~~~~~~~~~~~~~~')
                        print('Failed waiting for gRecaptchaResponse textarea')
                        print('~~~~~~~~~~~~~~~~~~~~~')
                        raise Exception('input')
                    gRecaptchaResponseTextarea = self.driver.find_element(By.XPATH, '//textarea[@id="g-recaptcha-response"]')
                    self.driver.execute_script("return arguments[0].style.display = 'block';", gRecaptchaResponseTextarea)

                    try:
                        submit = self.driver.find_element(By.XPATH, '//input[@name="submit"]')
                    except:
                        submit = False
                    site_key_elem = self.driver.find_element(By.XPATH, '//*[@data-sitekey]')
                    url = self.driver.find_element(By.XPATH, '//input[@name="continue"]').get_attribute('value')
                    site_key = site_key_elem.get_attribute('data-sitekey')
                    
                    task_type = 'NoCaptchaTaskProxyless'
                    key = 'e5ebc2bb0f61654782ca37d29c4c720b'
                    print('waiting for captcha')
                    agent = self.driver.execute_script("return navigator.userAgent")
                    # PROXY_USER = 'sqduser'
                    # PROXY_PASS = 'GyOjj4m1iaQbS'
                    # _tmp_proxy = proxy.replace('http://', '')
                    # PROXY_HOST = _tmp_proxy.split(':')[0]
                    # PROXY_PORT = _tmp_proxy.split(':')[1]
                    # print("PROXY REQUEST: ")
                    # print(self.ANTICAPTCHA_KEY,agent,"http",PROXY_HOST,PROXY_PORT,PROXY_USER,PROXY_PASS)
                    current_cookies = []
                    for cookie in self.driver.get_cookies():
                        current_cookies.append('{}={}'.format(cookie['name'], cookie['value']))
                    current_cookies = '; '.join(current_cookies)

                    res = await self.dbs['positions'].captcha_statistics.update({'ip' : proxy}, {'$inc' : {
                        'captcha_count' : 1
                    }}, safe = True, upsert = True)

                    solver = recaptchaV2Proxyless()
                    solver.set_verbose(1)
                    solver.set_key(self.ANTICAPTCHA_KEY)
                    solver.set_website_url(url)
                    solver.set_website_key(site_key)
                    try:
                        sToken = site_key_elem.get_attribute('data-s')
                        solver.set_data_s(sToken)
                    except:
                        pass
                    solver.set_user_agent(agent)
                    solver.set_cookies(current_cookies)
                    captcha_result = solver.solve_and_return_solution()
                    print('~~~~~~~~~ captcha result ~~~~~~~~~')
                    print(captcha_result)
                    print('~~~~~~~~~ captcha result ~~~~~~~~~')
                    if captcha_result == 'could_not_create_task':
                        _continue -= 1
                        await asyncio.sleep(5, loop = loop)
                        continue
                    if captcha_result == 0:
                        raise Exception('Capthcha response error')
                    if 'errorId' in captcha_result and captcha_result['errorId'] == 0:
                        print('captcha solved')
                        gresponse = captcha_result['solution']['gRecaptchaResponse']
                        if 'cookies' in captcha_result['solution'] and len(captcha_result['solution']['cookies']) > 0:
                            for cookie_name, cookie_value in captcha_result['solution']['cookies'].items():
                                self.driver.add_cookie({'name' : cookie_name, 'value' : cookie_value})
                        current_cookies = []
                        for cookie in self.driver.get_cookies():
                            current_cookies.append('{}={}'.format(cookie['name'], cookie['value']))
                        current_cookies = '; '.join(current_cookies)
                        print('~~~~~updated_cookies~~~~~')
                        print(current_cookies)
                        print('~~~~~updated_cookies~~~~~')
                        gRecaptchaResponseTextarea.send_keys(gresponse)
                        # self.driver.execute_script('return arguments[0].innertHTML = "{}"'.format(gresponse), gRecaptchaResponseTextarea)
                        await asyncio.sleep(.1, loop = loop)
                        self.driver.execute_script('submitCallback("{}");'.format(gresponse))
                        await asyncio.sleep(.1, loop = loop)
                        try:
                            WebDriverWait(self.driver, 10).until(lambda driver: self.driver.execute_script('return document.readyState') == 'complete')
                        except:
                            print('FAILED FOR WAITING DOCUMENT READY')
                            proxy = self.get_available_proxy('se_positions')
                            proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                            await self.get_driver(loop, proxy, proxy_md5)
                            continue
                            

                        print('CURRENT URL: {}'.format(self.driver.current_url))
                        if self.driver.current_url.endswith('sorry/index'):
                            proxy = self.get_available_proxy('se_positions')
                            proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                            await self.get_driver(loop, proxy, proxy_md5)
                            continue

                        captcha = False
                        if '/sorry/index?' in self.driver.current_url:
                            captcha = True
                        if captcha is not True:
                            body = self.driver.find_element(By.XPATH, '//body')
                            onload = body.get_attribute('onload')
                            if onload is not None and 'captcha' in onload:
                                captcha = True
                        if captcha is True:
                            raise Exception('Another captcha')
                            continue
                        else:
                            res = await self.dbs['positions'].captcha_statistics.update({'ip' : proxy}, {'$inc' : {
                                'captcha_count' : -1
                            }}, safe = True, upsert = True)
                    else:
                        if 'errorCode' in captcha_result:
                            if captcha_result['errorCode'] == 'ERROR_TOKEN_EXPIRED':
                                print('ERROR_TOKEN_EXPIRED | Retrying same task')
                                _continue = 100
                                raise Exception('Token expired')
                            elif captcha_result['errorCode'] == 'ERROR_CAPTCHA_UNSOLVABLE':
                                print(captcha_result['errorCode'])
                                raise Exception('Captcha unsolvable, trying again')
                            elif captcha_result['errorCode'] == 'ERROR_PROXY_BANNED' or captcha_result['errorCode'] == 'ERROR_PROXY_CONNECT_TIMEOUT' or captcha_result['errorCode'] == 'ERROR_PROXY_INCOMPATIBLE_HTTP_VERSION' or captcha_result['errorCode'] == 'ERROR_FAILED_LOADING_WIDGET':
                                _continue = 100
                                raise Exception('Proxy banned or timeouted {}'.format(proxy))
                            else:
                                print(captcha_result)
                                await asyncio.sleep(1, loop = loop)
                        else:
                            print(captcha_result)
                            await asyncio.sleep(1, loop = loop)'''
                if '/checkcaptcha' in self.driver.page_source and 'google' not in self.driver.current_url:
                    isYandex = True
                    print("YANDEX CAPTCHA")
                    captcha_img = self.driver.find_element(By.XPATH, '//img[contains(@class, "form__captcha")]').get_attribute('src')
                    captcha_result = await ImageToTextTask.aioImageToTextTask(anticaptcha_key=self.ANTICAPTCHA_KEY, language = 'en').captcha_handler(captcha_link = captcha_img)
                    if 'errorId' in captcha_result and captcha_result['errorId'] == 0:
                        yaresponse = captcha_result['solution']['text']
                        yacinput = self.driver.find_element(By.XPATH, '//input[@id="rep"]')
                        yacinput.send_keys(yaresponse)
                        yabutton = self.driver.find_element(By.XPATH, '//button[contains(@class,"form__submit")]')
                        yabutton.click()
                    print(captcha_result)
                else:
                    # print('no captcha')
                    break
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                tb = traceback.format_exc()
                if e != 'Another captcha' and e != 'Something wrong with captcha page':
                    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                    print('something fucked up {0}'.format(e))
                    print(exc_type, fname, exc_tb.tb_lineno, tb)
                    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                _continue += 1
                if _continue >= 3:
                    proxy = self.get_available_proxy('se_positions')
                    proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                    await self.get_driver(loop, proxy, proxy_md5)
                continue
        return self.driver.page_source


    async def check_recaptcha_new(self, loop, proxy, proxy_md5, isLogin = False, cookies = False):
        while True:
            print(' ~~~ CHECKING FOR RECAPTCHA ~~~ ')
            if '/sorry/index' in self.driver.current_url:
                if 'sorry/image' in self.driver.page_source:
                    await self.solve_image_captcha(loop, proxy, proxy_md5)
                elif self.driver.find_element(By.XPATH, '//div[@id="recaptcha"]'):
                    print(' ~~~ RECAPTCHA PAGE ~~~ ')
                    try:
                        wait = WebDriverWait(self.driver, 120)
                        wait.until(EC.presence_of_element_located((By.XPATH, '//*[contains(@class,"solved")][contains(@class,"antigate_solver")]')))
                    except:
                        continue
                    recaptchBlock = self.driver.find_element(By.XPATH, '//div[@id="recaptcha"]')
                    currentCookies = self.driver.get_cookies()
                    cookiesString = []
                    for cookie in currentCookies:
                        cookiesString.append('{}={}'.format(cookie['name'], cookie['value']))
                    cookiesString = '&'.join(cookiesString)
                    siteKey = recaptchBlock.get_attribute('data-sitekey')
                    dataS = recaptchBlock.get_attribute('data-s')

                    dataQ = self.driver.find_element(By.XPATH, '//input[@name="q"]').get_attribute('value')
                    dataContinue = self.driver.find_element(By.XPATH, '//input[@name="continue"]').get_attribute('value')

                    task_type = 'NoCaptchaTaskProxyless'
                    agent = self.driver.execute_script("return navigator.userAgent")
                    proxyInfo = proxy.split('@')
                    PROXY_USER, PROXY_PASS = proxyInfo[0].split(':')
                    PROXY_HOST, PROXY_PORT = proxyInfo[1].split(':')

                    solver = recaptchaV2Proxyon()
                    solver.set_verbose(1)
                    solver.set_key(self.ANTICAPTCHA_KEY)
                    solver.set_website_url(self.driver.current_url)
                    solver.set_website_key(siteKey)
                    solver.set_data_s(dataS)
                    # solver.set_enterprise_payload({'s' : dataS})
                    solver.set_user_agent(agent)

                    solver.set_proxy_address(PROXY_HOST)
                    solver.set_proxy_port(PROXY_PORT)
                    solver.set_proxy_login(PROXY_USER)
                    solver.set_proxy_password(PROXY_PASS)
                    solver.set_cookies(cookiesString)

                    print(' ~~~ TASK DATA ~~~ ')
                    print('website_url : {}'.format(self.driver.current_url))
                    print('website_key : {}'.format(siteKey))
                    print('data_s : {}'.format(dataS))
                    print('user_agent : {}'.format(agent))
                    print('proxy_address : {}'.format(PROXY_HOST))
                    print('proxy_port : {}'.format(PROXY_PORT))
                    print('proxy_login : {}'.format(PROXY_USER))
                    print('proxy_password : {}'.format(PROXY_PASS))
                    print('cookies : {}'.format(cookiesString))
                    print(' ~~~ TASK DATA ~~~ ')

                    taskResponse = solver.solve_and_return_solution()
                    if taskResponse != 0:
                        print('payload', {'clientKey' : self.ANTICAPTCHA_KEY, 'taskId' : solver.task_id})
                        taskResult = requests.post('https://api.anti-captcha.com/getTaskResult', json = {'clientKey' : self.ANTICAPTCHA_KEY, 'taskId' : solver.task_id})
                        captchaResult = taskResult.json()
                        print(captchaResult)
                        gresponse = captchaResult['solution']['gRecaptchaResponse']
                        if 'cookies' in captchaResult['solution'] and len(captchaResult['solution']['cookies']) > 0:
                            for cookie_name, cookie_value in captchaResult['solution']['cookies'].items():
                                if cookie_name == '' or cookie_value == '' or cookie_name == 'nocookies' or cookie_name == 'empty':
                                    continue
                                cookieReplaced = False
                                for cookie in currentCookies:
                                    if cookie['name'] == cookie_name:
                                        self.driver.delete_cookie(cookie['name'])
                                        cookie['value'] = cookie_value
                                        self.driver.add_cookie(cookie)
                                        cookieReplaced = True
                                        break
                                if cookieReplaced is False:
                                    self.driver.add_cookie({'name' : cookie_name, 'value' : cookie_value, 'domain' : '.google.com'})
                    else:
                        print(' ~~~ ERROR WHILE SOLVING ~~~ ')
                        print(solver.error_code)
                        print(' ~~~ ERROR WHILE SOLVING ~~~ ')
                        proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                        proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                        await self.get_driver(loop, proxy, proxy_md5)
                        continue
                    await asyncio.sleep(.5, loop = loop)
                    self.driver.execute_script("submitCallback('{}')".format(gresponse))
                else:
                    print(' ~~~ UNKNOWN CAPTCHA PAGE ~~~ ')
                    proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                    proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                    await self.get_driver(loop, proxy, proxy_md5)
                continue
            break
        return self.driver.page_source


    async def check_recaptcha(self, loop, proxy, proxy_md5, isLogin = False, cookies = False):
        _continue = 0
        isYandex = False
        while True:
            try:
                captcha = False
                if self.driver.current_url.endswith('sorry/index'):
                    proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                    proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                    await self.get_driver(loop, proxy, proxy_md5)
                    continue
                if '/sorry/index?' in self.driver.current_url:
                    captcha = True
                if captcha is not True:
                    try:
                        body = self.driver.find_element(By.XPATH, '//body')
                    except:
                        _continue = 100
                        raise Exception('Wrong response')
                    onload = body.get_attribute('onload')
                    if onload is not None and 'captcha' in onload:
                        captcha = True
                if captcha is True:
                    if 'sorry/image' in self.driver.page_source:
                        await self.solve_image_captcha(loop, proxy, proxy_md5)
                        continue
                    else:
                        print("\t\t\t\t\t\t\t HOLY CAPTCHA | CONTINUE: {0}".format(_continue))
                        content = self.driver.page_source
                        try:
                            wait = WebDriverWait(self.driver, 10)
                            wait.until(EC.presence_of_element_located((By.XPATH, '//textarea[@id="g-recaptcha-response"]')))
                        except:
                            print('~~~~~~~~~~~~~~~~~~~~~')
                            print('Failed waiting for gRecaptchaResponse textarea')
                            print('~~~~~~~~~~~~~~~~~~~~~')
                            _continue = 100
                            raise Exception('input')
                        gRecaptchaResponseTextarea = self.driver.find_element(By.XPATH, '//textarea[@id="g-recaptcha-response"]')
                        self.driver.execute_script("return arguments[0].style.display = 'block';", gRecaptchaResponseTextarea)

                        try:
                            submit = self.driver.find_element(By.XPATH, '//input[@name="submit"]')
                        except:
                            submit = False
                        site_key_elem = self.driver.find_element(By.XPATH, '//*[@data-sitekey]')
                        url = self.driver.find_element(By.XPATH, '//input[@name="continue"]').get_attribute('value')
                        site_key = site_key_elem.get_attribute('data-sitekey')
                        
                        task_type = 'NoCaptchaTaskProxyless'
                        key = 'e5ebc2bb0f61654782ca37d29c4c720b'
                        print('waiting for captcha')
                        agent = self.driver.execute_script("return navigator.userAgent")
                        PROXY_USER = 'sqduser'
                        PROXY_PASS = 'GyOjj4m1iaQbS'
                        _tmp_proxy = proxy.replace('http://', '')
                        PROXY_HOST = _tmp_proxy.split(':')[0]
                        PROXY_PORT = _tmp_proxy.split(':')[1]
                        # print("PROXY REQUEST: ")
                        # print(self.ANTICAPTCHA_KEY,agent,"http",PROXY_HOST,PROXY_PORT,PROXY_USER,PROXY_PASS)

                        current_cookies = []
                        currentCookiesAppendLater = []
                        currentCookiesDriver = self.driver.get_cookies()
                        if cookies is not False and len(cookies) > 0:
                            currentCookiesDriver = cookies
                        for cookie in currentCookiesDriver:
                            current_cookies.append('{}={}'.format(cookie['name'], cookie['value']))
                            currentCookiesAppendLater.append(cookie)
                        current_cookies = '; '.join(current_cookies)
                        print('~~~~~current_cookies~~~~~')
                        print(current_cookies)
                        print('~~~~~current_cookies~~~~~')
                        res = await self.dbs['positions'].captcha_statistics.update({'ip' : proxy}, {'$inc' : {
                            'captcha_count' : 1
                        }}, safe = True, upsert = True)

                        solver = recaptchaV2Proxyon()
                        solver.set_verbose(1)
                        solver.set_key(self.ANTICAPTCHA_KEY)
                        solver.set_website_url(self.driver.current_url)
                        solver.set_website_key(site_key)
                        sToken = site_key_elem.get_attribute('data-s')
                        solver.set_data_s(sToken)
                        solver.set_user_agent(agent)
                        solver.set_proxy_address(PROXY_HOST)
                        solver.set_proxy_port(PROXY_PORT)
                        solver.set_proxy_login(PROXY_USER)
                        solver.set_proxy_password(PROXY_PASS)
                        solver.set_cookies(current_cookies)

                        print('~~~ TASK DATA ~~~')
                        print('website_url : {}'.format(self.driver.current_url))
                        print('website_key : {}'.format(site_key))
                        print('data_s : {}'.format(sToken))
                        print('cookies : {}'.format(current_cookies))
                        print('~~~ TASK DATA ~~~')
                        captcha_result = solver.solve_and_return_solution()
                        print('~~~~~~~~~ captcha result ~~~~~~~~~')
                        print(captcha_result)
                        print('~~~~~~~~~ captcha result ~~~~~~~~~')
                        if captcha_result == 'could_not_create_task':
                            _continue -= 1
                            await asyncio.sleep(5, loop = loop)
                            continue
                        if captcha_result == 0:
                            raise Exception('Capthcha response error')
                        if 'errorId' in captcha_result and captcha_result['errorId'] == 0:
                            print('captcha solved')
                            gresponse = captcha_result['solution']['gRecaptchaResponse']
                            if 'cookies' in captcha_result['solution'] and len(captcha_result['solution']['cookies']) > 0:
                                for cookie_name, cookie_value in captcha_result['solution']['cookies'].items():
                                    if cookie_name != '' and cookie_value != '' and cookie_name != 'nocookies' and cookie_name != 'empty':
                                        self.driver.delete_cookie(cookie_name)
                                        self.driver.add_cookie({'name' : cookie_name, 'value' : cookie_value})
                                for oldCookie in currentCookiesAppendLater:
                                    self.driver.delete_cookie(oldCookie['name'])
                                    self.driver.add_cookie({'name' : oldCookie['name'], 'value' : oldCookie['value']})
                            current_cookies = []
                            for cookie in self.driver.get_cookies():
                                current_cookies.append('{}={}'.format(cookie['name'], cookie['value']))
                            current_cookies = '; '.join(current_cookies)
                            print('~~~~~updated_cookies~~~~~')
                            print(current_cookies)
                            print('~~~~~updated_cookies~~~~~')
                            # gRecaptchaResponseTextarea.send_keys(gresponse)
                            # self.driver.execute_script('return arguments[0].innertHTML = "{}"'.format(gresponse), gRecaptchaResponseTextarea)
                            await asyncio.sleep(.1, loop = loop)
                            # submitForm = self.driver.find_element(By.XPATH, '//form')
                            # submitForm.submit()
                            self.driver.execute_script('document.getElementById("g-recaptcha-response").innerHTML="{}";'.format(gresponse))
                            self.driver.execute_script('submitCallback("{}")'.format(gresponse))
                            await asyncio.sleep(.1, loop = loop)
                            try:
                                WebDriverWait(self.driver, 10).until(lambda driver: self.driver.execute_script('return document.readyState') == 'complete')
                            except:
                                print('FAILED FOR WAITING DOCUMENT READY')
                                proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                                proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                                await self.get_driver(loop, proxy, proxy_md5)
                                continue
                            continue

                            print('CURRENT URL: {}'.format(self.driver.current_url))
                            if self.driver.current_url.endswith('sorry/index'):
                                proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                                proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                                await self.get_driver(loop, proxy, proxy_md5)
                                continue

                            captcha = False
                            if '/sorry/index?' in self.driver.current_url:
                                captcha = True
                            if captcha is not True:
                                body = self.driver.find_element(By.XPATH, '//body')
                                onload = body.get_attribute('onload')
                                if onload is not None and 'captcha' in onload:
                                    captcha = True
                            if captcha is True:
                                print('Another Captcha - trying to solve')
                                continue
                            else:
                                res = await self.dbs['positions'].captcha_statistics.update({'ip' : proxy}, {'$inc' : {
                                    'captcha_count' : -1
                                }}, safe = True, upsert = True)
                        else:
                            if 'errorCode' in captcha_result:
                                if captcha_result['errorCode'] == 'ERROR_TOKEN_EXPIRED':
                                    print('ERROR_TOKEN_EXPIRED | Retrying same task')
                                    _continue = 100
                                    raise Exception('Token expired')
                                elif captcha_result['errorCode'] == 'ERROR_CAPTCHA_UNSOLVABLE':
                                    print(captcha_result['errorCode'])
                                    raise Exception('Captcha unsolvable, trying again')
                                elif captcha_result['errorCode'] == 'ERROR_PROXY_BANNED' or captcha_result['errorCode'] == 'ERROR_PROXY_CONNECT_TIMEOUT' or captcha_result['errorCode'] == 'ERROR_PROXY_INCOMPATIBLE_HTTP_VERSION' or captcha_result['errorCode'] == 'ERROR_FAILED_LOADING_WIDGET':
                                    _continue = 100
                                    raise Exception('Proxy banned or timeouted {}'.format(proxy))
                                else:
                                    print(captcha_result)
                                    await asyncio.sleep(1, loop = loop)
                            else:
                                print(captcha_result)
                                await asyncio.sleep(1, loop = loop)
                if '/checkcaptcha' in self.driver.page_source and 'google' not in self.driver.current_url:
                    isYandex = True
                    print("YANDEX CAPTCHA")
                    captcha_img = self.driver.find_element(By.XPATH, '//img[contains(@class, "form__captcha")]').get_attribute('src')
                    captcha_result = await ImageToTextTask.aioImageToTextTask(anticaptcha_key=self.ANTICAPTCHA_KEY, language = 'en').captcha_handler(captcha_link = captcha_img)
                    if 'errorId' in captcha_result and captcha_result['errorId'] == 0:
                        yaresponse = captcha_result['solution']['text']
                        yacinput = self.driver.find_element(By.XPATH, '//input[@id="rep"]')
                        yacinput.send_keys(yaresponse)
                        yabutton = self.driver.find_element(By.XPATH, '//button[contains(@class,"form__submit")]')
                        yabutton.click()
                    print(captcha_result)
                else:
                    # print('no captcha')
                    break
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                tb = traceback.format_exc()
                if e != 'Another captcha':
                    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                    print('something fucked up {0}'.format(e))
                    print(exc_type, fname, exc_tb.tb_lineno, tb)
                    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                _continue += 1
                if _continue >= 3:
                    proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
                    proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
                    await self.get_driver(loop, proxy, proxy_md5)
                continue
        return self.driver.page_source


    async def start_se_process(self, tasks, loop, worker_num, p_counter):
        worker_num = p_counter
        # proxy = None

        # cookies = {}
        # cookies = {'yp' : '1796217181.sp.nd:50'}
        # cookies = {'yp' : '1797755873.sp.nd%3A50'}
        # cookies = {'yp' : '1806236064.sp.nd%3A50'}
        # cookies = {'yp' : '1806236064.sp.nd%3A50#1787932361.yrts.1472572361#1494406668.ww.1#1505779289.szm.2:1920x1200:1920x1076'}
        # connector = aiohttp.TCPConnector(loop = loop, limit = 2)
        proxy = self.get_available_proxy('se_positions', 'positionsReports', True)
        proxy_md5 = hashlib.md5(proxy.encode('utf-8')).hexdigest()
        # self.driver.region = 'ua'

        return_data = []
        for (i, data) in enumerate(tasks):
            se_name = data['se_name']
            project = self.projects_data[data['report_id']]
            search_system = project['search_systems'][se_name]
            self.searchRegion = search_system.get('se_domain') or 'ua'
            self.searchRegion = self.searchRegion.split('.')[-1]
            print('WORKER {wn} ::: STARTING: {keyword} in {se}'.format(keyword = data['keyword'], se = se_name, wn = worker_num))
            if hasattr(self, 'driver') is False or self.driver is None:
                if se_name == 'yandex':
                    await self.get_driver_yandex(loop, proxy, proxy_md5)
                    captcha = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
                else:
                    await self.get_driver(loop, proxy, proxy_md5)
                    captcha = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
            if se_name == 'yandex' and self.driver.search_engine == 'google':
                await self.get_driver_yandex(loop, proxy, proxy_md5)
                captcha = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
            if se_name == 'google' and self.driver.search_engine == 'yandex':
                await self.get_driver_yandex(loop, proxy, proxy_md5)
                captcha = await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
            # try:
            #   session.cookie_jar.load('{}/{}.cookie'.format(self.proxy_cookies_dir, proxy_md5))
            #   print('\t\t\t\t\t\t\t\t\t\t\t ### LOAD PROXY !!! Co0k1E5 !!! ### - proxy {}'.format(proxy))
            # except:
                # pass

            while True:
                try:
                    print('WORKER {wn} ::: Processing: {keyword} in {se}'.format(keyword = data['keyword'], se = se_name, wn = worker_num))
                    project = self.projects_data[data['report_id']]
                    search_system = project['search_systems'][se_name]
#                   if search_system['se_domain'] != self.driver.region:
#                       print('Changing region to {}...'.format(search_system['se_domain']))
#                       self.driver.get('https://www.google.{0}'.format(search_system['se_domain']))
#                       await self.login(loop, search_system['se_domain'])
#                       self.driver.region = search_system['se_domain']

                    url = self.prepare_url(se_name, search_system, project['regionality'], data['keyword'])
                    results = await self.fetch_data(url, se_name, loop, project, data, proxy, proxy_md5)
                    if results is False:
                        continue
                    q_left = self.max_tasks_per_process - (i+1)
                    print('WORKER {wn} ::: FINISH: (left : {q_left}) {keyword} in {se} with {results}'.format(keyword = data['keyword'], q_left = q_left, se = se_name, results = len(results), wn = worker_num))
                    report_data = {
                        'report_id' : ObjectId(str(data['report_id'])),
                        'keyword_id' : ObjectId(str(data['_id'])),
                        'project_id' : ObjectId(str(project['_id'])),
                        'results' : results,
                        'date_add' : datetime.utcnow(),
                    }
                    
                    search_hash = hashlib.md5(''.join([
                        search_system.get('se_country') or '',
                        search_system.get('se_name') or '',
                        search_system.get('se_domain') or '',
                        search_system.get('se_region') or '',
                    ]).encode('utf-8')).hexdigest()
                    for (se_param, se_value) in search_system.items():
                        report_data[se_param] = se_value
                    report_data['se_hash'] = search_hash
                    return_data.append(report_data)
                    # print('WORKER {wn} ::: left ::: {left} ::: {process}'.format(wn = worker_num, left = len(tasks) - (i+1), process = multiprocessing.current_process()))
                    # await asyncio.sleep(self.sleep_after_query, loop = loop)
                    break
                except:
                    print("~~~~Restarting Driver~~~~")
                    self.driver.quit()
                    if se_name == 'google':
                        await self.get_driver(loop, proxy, proxy_md5)
                    else:
                        await self.get_driver_yandex(loop, proxy, proxy_md5)
                    await asyncio.sleep(3, loop = loop)
                    print("~~~~Checking For Captcha~~~~~")
                    await self.check_recaptcha_with_audio(loop, proxy, proxy_md5, True)
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    tb = traceback.format_exc()
                    print("~~~~Continue~~~~")
                    print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~query~~~~~~~~~~~~~")
                    print(exc_type, fname, exc_tb.tb_lineno, tb)
                    print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t~~~~~~~~~~~~query~~~~~~~~~~~~~")
                    continue


        # await asyncio.sleep(100000, loop = loop)
        #driver.service.process.send_signal(signal.SIGTERM)
        self.driver.quit()
        return return_data


    def init_process(self, pool, counter, p_counter):
        init_process_loop = asyncio.new_event_loop()
        asyncio.set_event_loop(init_process_loop)

        self.dbs = {}
        self.dbs['pager'] = init_process_loop.run_until_complete(asyncio_mongo.Connection.create(self.pager_ip, loop = init_process_loop)).newshield
        self.dbs['positions'] = init_process_loop.run_until_complete(asyncio_mongo.Connection.create(self.positions_ip, loop = init_process_loop)).newshield_positions

        init_process_loop.run_until_complete(self.start_process(pool, counter, init_process_loop, p_counter))
        init_process_loop.close()


asyncioReports()
